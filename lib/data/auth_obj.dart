import 'dart:convert';

class AuthObj {
  AuthObj({
    required this.user,
    required this.token,
    required this.refreshToken,
  });

  User user;
  String token;
  String refreshToken;

  factory AuthObj.fromJson(String str) => AuthObj.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory AuthObj.fromMap(Map<String, dynamic> json) => AuthObj(
    user: User.fromMap(json["user"]),
    token: json["token"],
    refreshToken: json["refreshToken"],
  );

  Map<String, dynamic> toMap() => {
    "user": user.toMap(),
    "token": token,
    "refreshToken": refreshToken,
  };
}

class User {
  User({
    required this.id,
    required this.firstname,
    required this.lastname,
    required this.phoneNumber,
    required this.verified,
    this.profilePic,
    required this.currentMode,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String firstname;
  String lastname;
  String phoneNumber;
  String verified;
  dynamic profilePic;
  String currentMode;
  String createdAt;
  String updatedAt;

  factory User.fromJson(String str) => User.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory User.fromMap(Map<String, dynamic> json) => User(
    id: json["id"],
    firstname: json["firstname"],
    lastname: json["lastname"],
    phoneNumber: json["phoneNumber"],
    verified: json["verified"],
    profilePic: json["profilePic"],
    currentMode: json["currentMode"],
    createdAt: json["createdAt"],
    updatedAt: json["updatedAt"],
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "firstname": firstname,
    "lastname": lastname,
    "phoneNumber": phoneNumber,
    "verified": verified,
    "profilePic": profilePic,
    "currentMode": currentMode,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
  };

  fullName() {
    return "$firstname $lastname";
  }
}
