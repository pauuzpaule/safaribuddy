import 'dart:convert';

import 'package:safaribuddy/data/trip.dart';

class BookedTrip {
  BookedTrip({
    required this.id,
    required this.userId,
    required this.tripId,
    required this.numberOfSeats,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.trip,
  });

  int id;
  int userId;
  int tripId;
  int numberOfSeats;
  String status;
  String createdAt;
  String updatedAt;
  Trip trip;

  factory BookedTrip.fromJson(String str) => BookedTrip.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BookedTrip.fromMap(Map<String, dynamic> json) => BookedTrip(
    id: json["id"],
    userId: json["userId"],
    tripId: json["tripId"],
    numberOfSeats: json["numberOfSeats"],
    status: json["status"],
    createdAt: json["createdAt"],
    updatedAt: json["updatedAt"],
    trip: Trip.fromMap(json["trip"]),
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "userId": userId,
    "tripId": tripId,
    "numberOfSeats": numberOfSeats,
    "status": status,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
    "trip": trip.toMap(),
  };

  static List<BookedTrip> listFromString(String trips) =>
      (json.decode(trips) as List<dynamic>)
          .map<BookedTrip>((item) => BookedTrip.fromJson(jsonEncode(item)))
          .toList();
}