// To parse this JSON data, do
//
//     final car = carFromMap(jsonString);

import 'dart:convert';

class Car {
  Car({
    required this.id,
    required this.userId,
    required this.numberOfSeats,
    required this.name,
    required this.model,
    required this.licensePlate,
    required this.type,
    required this.createdAt,
    required this.updatedAt,
    this.images,
  });

  int id;
  int userId;
  int numberOfSeats;
  String name;
  String model;
  String licensePlate;
  String type;
  String createdAt;
  String updatedAt;
  List<Image>? images;

  factory Car.fromJson(String str) => Car.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Car.fromMap(Map<String, dynamic> json) => Car(
    id: json["id"],
    userId: json["userId"],
    numberOfSeats: json["numberOfSeats"],
    name: json["name"],
    model: json["model"],
    licensePlate: json["licensePlate"],
    type: json["type"],
    createdAt: json["createdAt"],
    updatedAt: json["updatedAt"],
    images: json['images'] != null ? List<Image>.from(json["images"].map((x) => Image.fromMap(x))) : null,
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "userId": userId,
    "numberOfSeats": numberOfSeats,
    "name": name,
    "model": model,
    "licensePlate": licensePlate,
    "type": type,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
    "images": List<dynamic>.from(images!.map((x) => x.toMap())),
  };

  static List<Car> listFromString(String cars) =>
      (json.decode(cars) as List<dynamic>)
          .map<Car>((item) => Car.fromJson(jsonEncode(item)))
          .toList();
}

class Image {
  Image({
    required this.id,
    required this.carId,
    required this.image,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  int carId;
  String image;
  String createdAt;
  String updatedAt;

  factory Image.fromJson(String str) => Image.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Image.fromMap(Map<String, dynamic> json) => Image(
    id: json["id"],
    carId: json["carId"],
    image: json["image"],
    createdAt: json["createdAt"],
    updatedAt: json["updatedAt"],
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "carId": carId,
    "image": image,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
  };

}
