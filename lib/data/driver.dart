// To parse this JSON data, do
//
//     final driver = driverFromMap(jsonString);

import 'dart:convert';

class Driver {
  Driver({
    required this.id,
    required this.idImage,
    this.currentLat,
    this.currentLng,
    this.isApproved,
    this.accountBlocked,
    this.userId,
    this.createdAt,
    this.updatedAt,
    this.user,
  });

  int id;
  String? idImage;
  dynamic currentLat;
  dynamic currentLng;
  String? isApproved;
  String? accountBlocked;
  int? userId;
  String? createdAt;
  String? updatedAt;
  User? user;

  factory Driver.fromJson(String str) => Driver.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Driver.fromMap(Map<String, dynamic> json) => Driver(
    id: json["id"],
    idImage: json["idImage"],
    currentLat: json["currentLat"],
    currentLng: json["currentLng"],
    isApproved: json["isApproved"],
    accountBlocked: json["accountBlocked"],
    userId: json["userId"],
    createdAt: json["createdAt"],
    updatedAt: json["updatedAt"],
    user: json["user"] != null ? User.fromMap(json["user"]) : null,
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "idImage": idImage,
    "currentLat": currentLat,
    "currentLng": currentLng,
    "isApproved": isApproved,
    "accountBlocked": accountBlocked,
    "userId": userId,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
    "user": user!.toMap(),
  };
}

class User {
  User({
    required this.firstname,
    required this.lastname,
    required this.phoneNumber,
    this.profilePic,
  });

  String firstname;
  String lastname;
  String phoneNumber;
  dynamic profilePic;

  factory User.fromJson(String str) => User.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory User.fromMap(Map<String, dynamic> json) => User(
    firstname: json["firstname"],
    lastname: json["lastname"],
    phoneNumber: json["phoneNumber"],
    profilePic: json["profilePic"],
  );

  Map<String, dynamic> toMap() => {
    "firstname": firstname,
    "lastname": lastname,
    "phoneNumber": phoneNumber,
    "profilePic": profilePic,
  };
}
