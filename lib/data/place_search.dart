import 'dart:convert';

class PlaceSearch {
  PlaceSearch({
    required this.description,
    required this.placeId,
  });

  String description;
  String placeId;

  factory PlaceSearch.fromJson(String str) => PlaceSearch.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PlaceSearch.fromMap(Map<String, dynamic> json) => PlaceSearch(
    description: json["description"],
    placeId: json["placeId"],
  );

  Map<String, dynamic> toMap() => {
    "description": description,
    "placeId": placeId,
  };


  static String encode(List<PlaceSearch> places) => json.encode(
    places
        .map<Map<String, dynamic>>((place) => place.toMap())
        .toList(),
  );

  static List<PlaceSearch> decode(String places) =>
      (json.decode(places) as List<dynamic>)
          .map<PlaceSearch>((item) => PlaceSearch.fromJson(jsonEncode(item)))
          .toList();
}
