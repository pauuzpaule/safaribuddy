class RecentDestination {
  var address, district;

  RecentDestination({this.address, this.district});

  List<RecentDestination> dummyData() {
    List<RecentDestination> list = [];
    list.add(RecentDestination(address: "Wandegeya", district: "Kampala"));
    list.add(RecentDestination(address: "Kololo", district: "Kampala"));
    list.add(RecentDestination(address: "Dfcu Branch", district: "Jinja"));
    /*for(int i =0; i < 10; i++) {
      list.add(RecentDestination(address: "Wandegeya", district: "Kampala"));
    }*/
    return list;
  }
}