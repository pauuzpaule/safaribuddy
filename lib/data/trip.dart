// To parse this JSON data, do
//
//     final trip = tripFromMap(jsonString);

import 'dart:convert';

import 'package:safaribuddy/util/utils.dart';

class Trip {
  Trip({
    required this.id,
    this.userId,
    required this.type,
    required this.numberOfSeats,
    this.pricePerSeat,
    required this.pickupAddress,
    required this.dropOffAddress,
    this.driverId,
    required this.status,
    required this.initiatedAs,
    required this.createdAt,
    required this.updatedAt,
    required this.pickupTime,
    required this.takenSeats,
    required this.dropofflat,
    required this.dropofflng,
    required this.pickuplat,
    required this.pickuplng,
    this.tripBids,
    this.driver,
  });

  int id;
  int? userId;
  String type;
  int numberOfSeats;
  int? pricePerSeat;
  String pickupAddress;
  String dropOffAddress;
  int? driverId;
  String status;
  String initiatedAs;
  String createdAt;
  String updatedAt;
  String pickupTime;
  int takenSeats;
  double dropofflat;
  double dropofflng;
  double pickuplat;
  double pickuplng;
  Driver? driver;
  List<TripBid>? tripBids;

  factory Trip.fromJson(String str) => Trip.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Trip.fromMap(Map<String, dynamic> json) => Trip(
    id: json["id"],
    userId: json["userId"],
    type: json["type"],
    numberOfSeats: json["numberOfSeats"],
    pricePerSeat: json["pricePerSeat"] ?? 0,
    pickupAddress: json["pickupAddress"],
    dropOffAddress: json["dropOffAddress"],
    driverId: json["driverId"] ?? null,
    status: json["status"],
    initiatedAs: json["initiatedAs"],
    createdAt: json["createdAt"],
    updatedAt: json["updatedAt"],
    pickupTime: json["pickupTime"],
    takenSeats: json["takenSeats"],
    dropofflat: json["dropofflat"],
    dropofflng: json["dropofflng"],
    pickuplat: json["pickuplat"],
    pickuplng: json["pickuplng"],
    tripBids: json["tripBids"] != null  ? List<TripBid>.from(json["tripBids"].map((x) => TripBid.fromMap(x))) : null,
    driver: json["driver"] != null ? Driver.fromMap(json["driver"]) : null,
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "userId": userId,
    "type": type,
    "numberOfSeats": numberOfSeats,
    "pricePerSeat": pricePerSeat,
    "pickupAddress": pickupAddress,
    "dropOffAddress": dropOffAddress,
    "driverId": driverId,
    "status": status,
    "initiatedAs": initiatedAs,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
    "pickupTime": pickupTime,
    "takenSeats": takenSeats,
    "dropofflat": dropofflat,
    "dropofflng": dropofflng,
    "pickuplat": pickuplat,
    "pickuplng": pickuplng,
    "tripBids": List<dynamic>.from(tripBids!.map((x) => x.toMap())),
    "driver": driver!.toMap(),
  };

  formatPickupDate({format = 'yyyy-MM-ddTHH:mm:ss+00:00', outputFormat = "E, d/MMM/yy  HH:mm"}) {

    try{
      var dateLocal = dateFromUtcToLocal(pickupTime, format: format);
      return dateFormat(dateLocal, format: outputFormat);
    }catch (e) {
      var dateLocal = dateFromUtcToLocal(pickupTime, format: 'yyyy-MM-ddTHH:mm:ss.000Z');
      return dateFormat(dateLocal, format: outputFormat);
    }

  }


  availableSeats() {
    return  numberOfSeats - takenSeats;
  }

  static List<Trip> listFromString(String trips) =>
      (json.decode(trips) as List<dynamic>)
          .map<Trip>((item) => Trip.fromJson(jsonEncode(item)))
          .toList();
}

class Driver {
  Driver({
    required this.user,
  });

  User user;

  factory Driver.fromJson(String str) => Driver.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Driver.fromMap(Map<String, dynamic> json) => Driver(
    user: User.fromMap(json["user"]),
  );

  Map<String, dynamic> toMap() => {
    "user": user.toMap(),
  };
}

class User {
  User({
    required this.firstname,
    required this.lastname,
    required this.phoneNumber,
    this.profilePic,
  });

  String firstname;
  String lastname;
  String phoneNumber;
  dynamic profilePic;

  factory User.fromJson(String str) => User.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory User.fromMap(Map<String, dynamic> json) => User(
    firstname: json["firstname"],
    lastname: json["lastname"],
    phoneNumber: json["phoneNumber"],
    profilePic: json["profilePic"],
  );

  Map<String, dynamic> toMap() => {
    "firstname": firstname,
    "lastname": lastname,
    "phoneNumber": phoneNumber,
    "profilePic": profilePic,
  };

  fullName() {
    return firstname + " " + lastname;
  }
}

class TripBid {
  TripBid({
    required this.id,
    required this.tripId,
    required this.driverId,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    this.driver
  });

  int id;
  int tripId;
  int driverId;
  String status;
  String createdAt;
  String updatedAt;
  Driver? driver;

  factory TripBid.fromJson(String str) => TripBid.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TripBid.fromMap(Map<String, dynamic> json) => TripBid(
    id: json["id"],
    tripId: json["tripId"],
    driverId: json["driverId"],
    status: json["status"],
    createdAt: json["createdAt"],
    updatedAt: json["updatedAt"],
    driver: json["driver"] != null ? Driver.fromMap(json["driver"]) : null,
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "tripId": tripId,
    "driverId": driverId,
    "status": status,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
    "driver": driver!.toMap(),
  };
}
