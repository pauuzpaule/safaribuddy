import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/providers/walk_through_provider.dart';
import 'package:safaribuddy/styles/theme_data.dart';
import 'package:safaribuddy/ui/add_boat_cruise_screen.dart';
import 'package:safaribuddy/ui/add_car_for_hire_screen.dart';
import 'package:safaribuddy/ui/add_delivery_details_screen.dart';
import 'package:safaribuddy/ui/available_trips.dart';
import 'package:safaribuddy/ui/boat_cruise/boat_cruise_details_screen.dart';
import 'package:safaribuddy/ui/boat_cruise/boat_cruise_list_screen.dart';
import 'package:safaribuddy/ui/booked_trips_screen.dart';
import 'package:safaribuddy/ui/car_hire_details_screen.dart';
import 'package:safaribuddy/ui/car_hire_list_screen.dart';
import 'package:safaribuddy/ui/chat_screen.dart';
import 'package:safaribuddy/ui/common/otp_verification_screen.dart';
import 'package:safaribuddy/ui/create_car_screen.dart';
import 'package:safaribuddy/ui/create_trip_driver_screen.dart';
import 'package:safaribuddy/ui/create_trip_passenger_screen.dart';
import 'package:safaribuddy/ui/driver_rides.dart';
import 'package:safaribuddy/ui/favorites_screen.dart';
import 'package:safaribuddy/ui/location_search_screen.dart';
import 'package:safaribuddy/ui/login_screen.dart';
import 'package:safaribuddy/ui/my_rides.dart';
import 'package:safaribuddy/ui/notification_screen.dart';
import 'package:safaribuddy/ui/on_trip_screen.dart';
import 'package:safaribuddy/ui/package_details_screen.dart';
import 'package:safaribuddy/ui/packages_list_screen.dart';
import 'package:safaribuddy/ui/payment_screen.dart';
import 'package:safaribuddy/ui/post_package_delivery_screen.dart';
import 'package:safaribuddy/ui/profile.dart';
import 'package:safaribuddy/ui/promotion_screen.dart';
import 'package:safaribuddy/ui/register_screen.dart';
import 'package:safaribuddy/ui/schedule_trip_location_screen.dart';
import 'package:safaribuddy/ui/schedule_trip_screen.dart';
import 'package:safaribuddy/ui/search_rider_screen.dart';
import 'package:safaribuddy/ui/splash_screen.dart';
import 'package:safaribuddy/ui/trip_details_screen.dart';
import 'package:safaribuddy/ui/trip_requests_screen.dart';
import 'package:safaribuddy/ui/user_cars_screen.dart';
import 'package:safaribuddy/ui/walkthrough.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/viewModel/auth_view_model.dart';
import 'package:safaribuddy/viewModel/car_view_model.dart';
import 'package:safaribuddy/viewModel/trip_view_model.dart';
import 'package:safaribuddy/widgets/location_picker_screen.dart';

void main() {
  runApp(
      Phoenix(
        child: const MyApp(),
      )
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<WalkThroughProvider>(create: (BuildContext context)  => WalkThroughProvider()),
        ChangeNotifierProvider<AuthViewModel>(create: (BuildContext context)  => AuthViewModel()),
        ChangeNotifierProvider<CarViewModel>(create: (BuildContext context)  => CarViewModel()),
        ChangeNotifierProvider<TripViewModel>(create: (BuildContext context)  => TripViewModel()),
      ],
      child: MaterialApp(
        title: kAppName,
        theme: ThemeScheme.basic(),
        debugShowCheckedModeBanner: false,
        routes: <String, WidgetBuilder>{

          homeRoute: (context) => const ScheduleTripScreen(),
          onTripRoute: (context) => const OnTripScreen(),
          loginRoute: (context) => const LoginScreen(),
          registerRoute: (context) => const RegisterScreen(),
          walkThroughRoute: (context) => WalkThroughScreen(),
          myRides: (context) => const MyRidesScreen(),
          driverRides: (context) => DriverRidesScreen(),
          rideDetails: (context) => const TripDetailsScreen(),
          searchRider: (context) => SearchRiderScreen(),
          profileRoute: (context) =>  const ProfileScreen(),
          otpVerificationRoute: (context) =>  const OtpVerificationScreen(),
          paymentsRoute: (context) =>  PaymentScreen(),
          pickLocationRoute: (context) =>  LocationPickerScreen(),
          chatDetailsRoute: (context) =>  ChatScreen(),
          promotionRoutes: (context) =>  const PromotionScreen(),
          favouriteScreen: (context) =>  FavoritesScreen(),
          notificationScreen : (context) =>  NotificationScreen(),
          availableRidesRoute : (context) =>  const AvailableTripsScreen(),
          locationSearchScreen : (context) =>  const LocationSearchScreen(),
          tripLocationRoute : (context) =>  const ScheduleTripLocationScreen(),
          createTripPassengerRoute : (context) =>  const CreateTripPassengerScreen(),
          createTripDriverRoute : (context) =>  const CreateTripDriverScreen(),
          tripRequestRoute : (context) =>  const TripRequestsScreen(),
          boatCruiseListRoute : (context) =>  const BoatCruiseListScreen(),
          boatCruiseDetailsRoute : (context) =>  const BoatCruiseDetailsScreen(),
          carHireListRoute : (context) =>  const CarHireListScreen(),
          carHireDetailsRoute : (context) =>  const CarHireDetailsScreen(),
          addCarForHireRoute : (context) =>  const AddCarForHireScreen(),
          addDeliveryDetailsRoute : (context) =>  const AddDeliveryDetailsScreen(),
          postPackageDeliveryRoute : (context) =>  const PostPackageDeliveryScreen(),
          addBoatCruiseRoute : (context) =>  const AddBoatCruiseScreen(),
          packagesListRoute : (context) =>  const PackagesListScreen(),
          packageDetailsRoute : (context) =>  const PackageDetailsScreen(),
          createCarRoute : (context) =>  const CreateCarScreen(),
          myCarsRoute : (context) =>  const UserCarsScreen(),
          bookedTripsRoute : (context) =>  const BookedTripsScreen(),
        },
        home:  const SplashScreen(),
        //home:  const LocationSearchScreen(),
      ),
    );
  }
}
