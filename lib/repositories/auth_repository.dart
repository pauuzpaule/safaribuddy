import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:safaribuddy/data/auth_obj.dart';
import 'package:safaribuddy/util/connection.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/util/utils.dart';

class AuthRepository {

  BuildContext context;
  AuthRepository(this.context);

  Future<dynamic> registerUser(request) async {
    try {
      var response = await makePostCall("/auth/register", request);

      if(response.statusCode <= 201) {
        var res = jsonDecode(response.body);
        return User.fromJson(jsonEncode(res["data"]["user"]));
      }else {
        return jsonDecode(response.body)["message"];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      if(e is SocketException) {
        showSnackBar(
            context, "Connection failed", action: SnackBarAction(label: "Try again", onPressed: () async {
          registerUser(request);
        }));

        return kInternetError;
      }


    }

    return kFailed;
  }

  Future<dynamic> loginUser(request) async {
    try {
      var response = await makePostCall("/auth/login", request);

      if(response.statusCode <= 201) {
        var res = jsonDecode(response.body);
        AuthObj obj = AuthObj.fromJson(jsonEncode(res["data"]));
        await SharedPrefs().storeAuthObj(obj);
        await SharedPrefs().saveUserType(obj.user.currentMode);
        return obj;
      }else {
        return jsonDecode(response.body)["message"];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      if(e is SocketException) {
        showSnackBar(
            context, "Connection failed", action: SnackBarAction(label: "Try again", onPressed: () async {
          loginUser(request);
        }));

        return kInternetError;
      }


    }

    return kFailed;
  }

  Future<String> verifyOtp(request) async {
    try {
      var response = await makePostCall("/auth/verify-account", request);

      if(response.statusCode <= 201) {
        return kSuccess;
      }else {
        return jsonDecode(response.body)["message"];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      if(e is SocketException) {
        showSnackBar(
            context, "Connection failed", action: SnackBarAction(label: "Try again", onPressed: () async {
          verifyOtp(request);
        }));
      }
    }

    return kFailed;
  }

}