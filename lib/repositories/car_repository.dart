import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:safaribuddy/data/car.dart';
import 'package:safaribuddy/util/connection.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/utils.dart';

class CarRepository {

  BuildContext context;
  CarRepository(this.context);

  Future<dynamic> registerCar(request) async {
    try {
      var response = await makePostCall("/car/create", request);

      if(response.statusCode <= 201) {
        var res = jsonDecode(response.body);
        return Car.fromJson(jsonEncode(res["data"]));
      }else {
        return jsonDecode(response.body)["message"];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      if(e is SocketException) {
        showSnackBar(
            context, "Connection failed", action: SnackBarAction(label: "Try again", onPressed: () async {
          registerCar(request);
        }));

        return kInternetError;
      }

    }
    return kFailed;
  }

  Future<String> uploadCarImage(carId, File file) async {
    try {
      var req = await makeMultipartCall("/car/upload-pic", file, body: {"carId": carId  }, method: "POST", fileParam: "file");

      var response = jsonDecode(await req.stream.bytesToString());
      print(response);
      if(response["status"] == "success") {
        return kSuccess;
      }else {
        return jsonDecode(response.body)["message"];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      if(e is SocketException) {
        return kInternetError;
      }else {
        return kServerError;
      }

    }
  }


  Future<List<Car>> getUserCars() async {
    try {
      var response = await makeCall("/car/my-cars");

      var res = jsonDecode(response.body);
      print(response.body);
      if(response.statusCode == 200) {
        return Car.listFromString(jsonEncode(res["data"]));
      }else {
        return [];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      return [];

    }
  }
  

}