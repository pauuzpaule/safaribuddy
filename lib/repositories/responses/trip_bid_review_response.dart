import 'dart:convert';

import 'package:safaribuddy/data/trip.dart';

class TripBidReviewResponse {
  TripBidReviewResponse({
    required this.trip,
    required this.tripBid,
  });

  Trip trip;
  TripBid tripBid;

  factory TripBidReviewResponse.fromJson(String str) => TripBidReviewResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TripBidReviewResponse.fromMap(Map<String, dynamic> json) => TripBidReviewResponse(
    trip: Trip.fromMap(json["trip"]),
    tripBid: TripBid.fromMap(json["tripBid"]),
  );

  Map<String, dynamic> toMap() => {
    "trip": trip.toMap(),
    "tripBid": tripBid.toMap(),
  };
}