import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:safaribuddy/data/booked_trip.dart';
import 'package:safaribuddy/data/trip.dart';
import 'package:safaribuddy/repositories/responses/trip_bid_review_response.dart';
import 'package:safaribuddy/util/connection.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/utils.dart';

class TripRepository {
  BuildContext context;
  TripRepository(this.context);

  Future<dynamic> createTrip(request) async {
    try {
      var response = await makePostCall("/trip/create", request);

      if(response.statusCode <= 201) {
        var res = jsonDecode(response.body);
        return Trip.fromJson(jsonEncode(res["data"]));
      }else {
        return jsonDecode(response.body)["message"];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      if(e is SocketException) {
        showSnackBar(
            context, "Connection failed", action: SnackBarAction(label: "Try again", onPressed: () async {
          createTrip(request);
        }));

        return kInternetError;
      }

    }
    return kFailed;
  }

  Future<List<Trip>> getAvailableTrips(pickupLat, pickupLng, dropOffLat, dropOffLng) async {
    try {
      var response = await makeCall("/trip/available-trips?pickuplat=$pickupLat&pickuplng=$pickupLng&dropofflat=$dropOffLat&dropofflng=$dropOffLng");

      var res = jsonDecode(response.body);
      print(response.body);
      if(response.statusCode == 200) {
        return Trip.listFromString(jsonEncode(res["data"]));
      }else {
        return [];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      return [];

    }
  }

  Future<List<Trip>> getTrips() async {
    try {
      var response = await makeCall("/trip/user-trips");

      var res = jsonDecode(response.body);
      print(response.body);
      if(response.statusCode == 200) {
        return Trip.listFromString(jsonEncode(res["data"]));
      }else {
        return [];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      return [];
    }
  }

  Future<List<BookedTrip>> getBookedTrips({status = 'pending'}) async {
    try {
      var response = await makeCall("/trip/booked-trips?status=$status");


      var res = jsonDecode(response.body);
      print(response.body);
      if(response.statusCode == 200) {
        return BookedTrip.listFromString(jsonEncode(res["data"]));
      }else {
        return [];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      return [];
    }
  }

  Future<dynamic> reviewBid(request) async {
    try {
      var response = await makePostCall("/trip/review-bid", request);

      if(response.statusCode <= 201) {
        var res = jsonDecode(response.body);
        return TripBidReviewResponse.fromJson(jsonEncode(res["data"]));
      }else {
        return jsonDecode(response.body)["message"];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      if(e is SocketException) {
        showSnackBar(
            context, "Connection failed", action: SnackBarAction(label: "Try again", onPressed: () async {
          reviewBid(request);
        }));

        return kInternetError;
      }

    }
    return kFailed;
  }

  Future<String> bookTrip(request) async {
    try {
      var response = await makePostCall("/trip/book", request);

      if(response.statusCode <= 201) {
        var res = jsonDecode(response.body);
        return kSuccess;
      }else {
        return jsonDecode(response.body)["message"];
      }
    }catch(e, trace) {
      print(e);
      print(trace);
      if(e is SocketException) {
        showSnackBar(
            context, "Connection failed", action: SnackBarAction(label: "Try again", onPressed: () async {
          bookTrip(request);
        }));

        return kInternetError;
      }

    }
    return kFailed;
  }


}