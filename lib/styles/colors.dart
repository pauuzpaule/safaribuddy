import 'package:flutter/material.dart';

final Color lprimaryColor = const Color(0xffEE4B45);
final Color lsecondaryColor = const Color(0xffFFAA00);
final Color lbasicDarkColor = const Color(0xff222B45);
final Color lbasicGreyColor = const Color(0xff8F9BB3);
final Color lbackgroundColor = const Color(0xffF7F9FC);

final Color bprimaryColor = const Color(0xff17333F);
final Color bsecondaryColor = const Color(0xffFF7D5C);
final Color bbasicDarkColor = const Color(0xff255164);
final Color bbasicGreyColor = const Color(0xff8F9BB3);
final Color bbackgroundColor = const Color(0xffF7F9FC);

final Color dprimaryColor = const Color(0xffEE4B45);
final Color dsecondaryColor = const Color(0xffFFAA00);
final Color dbasicDarkColor = const Color(0xff222B45);
final Color dbasicGreyColor = const Color(0xff8F9BB3);
final Color dbackgroundColor = const Color(0xff222B45);

final Color facebookColor = const Color(0xff3B5998);
