import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/widgets/form_widgets.dart';

class AddBoatCruiseScreen extends StatefulWidget {
  const AddBoatCruiseScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<AddBoatCruiseScreen> {

  String hireType = kSelfDrive;

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "Post Boat Cruise",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: ListView(
          children: [
            const SizedBox(height: 5,),
            textFieldInputRounded(hintText: "Title"),
            const SizedBox(height: 10,),
            textFieldInputRounded(hintText: "Start "),
            const SizedBox(height: 10,),
            textFieldInputRounded(hintText: "Destination"),
            const SizedBox(height: 10,),
            textFieldInputRounded(hintText: "Ticket price"),
            const SizedBox(height: 10,),
            textFieldInputRounded(
                readOnly: true,
                suffixIcon:  const Icon(
                  Icons.access_time_outlined,
                ),
                hintText: "Date"),
            const SizedBox(height: 10,),
            textAreaRounded(hintText: "Description"),
            const SizedBox(height: 10,),
            Container(
              height: 90,
              child: Row(
                children: [
                  InkWell(
                    child: Container(
                      height: 80,
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: const [
                          SizedBox(height: 5,),
                          Icon(Icons.add),
                          Text("Add Photo")
                        ],
                      ),
                    ),
                  ),

                  const SizedBox(width: 10,),
                  Expanded(
                    child: ListView.separated(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: 15,
                      separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(
                        width: 10,
                      ),
                      itemBuilder: (BuildContext context, int index) => ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.asset(
                          "assets/images/boat.jpeg",
                          height: 150.0,
                          width: 100.0,
                        ),
                      ),
                    ),
                  )

                ],
              ),
            ),

            const SizedBox(height: 30,),
            formButton(
                label: "POST",
                verticalPadding: 15.0,
                borderRadius: 10.0,
                borderColor: _theme.primaryColor,
                color: _theme.primaryColor,
                onPressed: () {
                  aweSomeDialog(context: context,
                      dialogType: DialogType.SUCCES,
                      title: "Successful",
                      desc: "Boa cruise posted.\n We shall contact you shortly.",
                      btnOkPress: () {
                        Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
                      });
                })
          ],
        ),
      ),
    );
  }
}
