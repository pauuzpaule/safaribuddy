import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/widgets/dash.dart';
import 'package:safaribuddy/widgets/form_widgets.dart';

class AddDeliveryDetailsScreen extends StatefulWidget {
  const AddDeliveryDetailsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<AddDeliveryDetailsScreen> {
  String tripType = kPassengerTrip;
  String deliveryType = kPackage;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Stack(
            children: [
              GoogleMap(
                myLocationButtonEnabled: false,
                myLocationEnabled: true,
                zoomControlsEnabled: false,
                initialCameraPosition: const CameraPosition(
                  target: LatLng(0.3247348, 32.5514714),
                  zoom: 16.0,
                ),
                onMapCreated: (GoogleMapController controller) async {},
              ),
              Positioned.fill(top: 100, child: _chooseTripView())
            ],
          ),
        ),
      ),
    );
  }

  _chooseTripView() {
    final ThemeData _theme = Theme.of(context);
    return Container(
      decoration: const BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30))),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                const Text(
                  "Delivery Details",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                const Spacer(),
                InkWell(
                  child: const Icon(Icons.close, color: Colors.white),
                  onTap: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, homeRoute, (route) => false);
                  },
                )
              ],
            ),
          ),
          Expanded(
              child: Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40))),
                child: ListView(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: const [
                        Text(
                          "Route",
                          style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    _locationView(),
                    const SizedBox(
                      height: 15,
                    ),
                    Divider(
                      color: Colors.grey[500],
                    ),

                    Container(
                      margin: const EdgeInsets.only(bottom: 15),
                      child: Row(
                        children: [
                          Expanded(child: Row(
                            children: [
                              Radio(
                                fillColor: MaterialStateColor.resolveWith(
                                        (states) => _theme.primaryColor),
                                value: kPackage,
                                groupValue: deliveryType,
                                onChanged: (value) {
                                  setState(() {
                                    deliveryType = value as String;
                                  });
                                },
                                activeColor: Colors.green,
                              ),
                              const Text("Package"),
                            ],
                          ),),
                          Expanded(child: Row(
                            children: [
                              Radio(
                                fillColor: MaterialStateColor.resolveWith(
                                        (states) => _theme.primaryColor),
                                value: kCargo,
                                groupValue: deliveryType,
                                onChanged: (value) {
                                  setState(() {
                                    deliveryType = value as String;
                                  });
                                },
                                activeColor: Colors.green,
                              ),
                              const Text("Cargo"),
                            ],
                          ),),
                        ],
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.only(bottom: 20),
                      child: Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: 'Estimated Price: '),
                            TextSpan(text: ' UGX 45,000', style: TextStyle(fontWeight: FontWeight.bold)),
                          ]
                        ),
                      ),
                    ),

                    textFieldInputRounded(hintText: "Recipient Name"),
                    const SizedBox(height: 10,),
                    textFieldInputRounded(hintText: "Recipient Phone Number (+256)"),
                    const SizedBox(height: 10,),
                    Visibility(
                      visible: deliveryType == kCargo,
                        child: Container(
                          margin: const EdgeInsets.only(bottom: 10),
                          child:  textFieldInputRounded(hintText: "Weight of Cargo (Kgs)", keyboardType: const TextInputType.numberWithOptions()),
                        )),
                    textFieldInputRounded(hintText: "Extra information"),
                    const SizedBox(height: 10,),
                    textFieldInputRounded(
                      readOnly: true,
                        suffixIcon:  const Icon(
                          Icons.access_time_outlined,
                        ),
                        hintText: "Pick up time"),
                    const SizedBox(height: 50,),
                    Row(
                      children: [
                        Container(
                            padding: const EdgeInsets.all(16),
                            decoration: ShapeDecoration(
                              color: Colors.grey[200],
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                            ),
                            child: InkWell(
                              child: const Icon(
                                Icons.arrow_back,
                              ),
                              onTap: () {
                                Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
                              },
                            )),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: formButton(
                                verticalPadding: 20.0,
                                borderRadius: 10.0,
                                textSize: 16,
                                borderColor: Colors.black,
                                color: Colors.black,
                                label: "Confirm",
                                onPressed: () {
                                  aweSomeDialog(context: context,
                                      dialogType: DialogType.SUCCES,
                                      title: "Confirmed",
                                      desc: "Delivery details received.\n We shall contact you shortly.",
                                      btnOkPress: () {

                                        Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
                                      });
                                }))
                      ],
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }

  _locationView() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 15,
              width: 15,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 5, color: Colors.black)),
            ),
            const SizedBox(
              width: 20,
            ),
            InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text("Pick up",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.w700)),
                  SizedBox(
                    height: 3,
                  ),
                  Text("kololo Airstrip, Kampala", style: TextStyle()),
                ],
              ),
              onTap: () {},
            )
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.only(left: 7),
              child: Dash(
                  direction: Axis.vertical,
                  length: 35,
                  dashLength: 1,
                  dashGap: 0,
                  dashColor: Colors.black),
            ),
            const SizedBox(
              width: 15,
            ),
            Expanded(
              child: Divider(
                height: 3,
                color: Colors.grey[200],
              ),
            )
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 15,
              width: 15,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 5, color: Colors.black)),
              child: Container(
                height: 20,
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text("Drop off",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.w700)),
                  SizedBox(
                    height: 3,
                  ),
                  Text("Makerere Rd, Kampala", style: TextStyle()),
                ],
              ),
              onTap: () {},
            )
          ],
        )
      ],
    );
  }
}
