import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/data/picked_place.dart';
import 'package:safaribuddy/data/trip.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/viewModel/trip_view_model.dart';
import 'package:safaribuddy/widgets/ride_card.dart';
import 'package:safaribuddy/widgets/ride_cards.dart';

class AvailableTripsScreen extends StatefulWidget {
  const AvailableTripsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<AvailableTripsScreen> {


  PickedPlace? pickUpLocation, dropOffLocation;
  bool isPassenger = true;

  List<Trip> availableTrips = [];


  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      SharedPrefs().getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });

      try{
        var intentData = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
        if(intentData.containsKey("pickUpLocation")) {
          pickUpLocation = intentData["pickUpLocation"];
        }
        if(intentData.containsKey("dropOffLocation")) {
          dropOffLocation = intentData["dropOffLocation"];
        }

        getAvailableTrips();
      }catch(e) {}
    });
    super.initState();
  }


  getAvailableTrips() {
    TripViewModel tripViewModel = Provider.of<TripViewModel>(context, listen: false);
    tripViewModel.getAvailableTrips(pickUpLocation?.lat, pickUpLocation?.lng, dropOffLocation?.lat, dropOffLocation?.lng, context: context).whenComplete(() {
      setState(() {
        availableTrips = tripViewModel.tripList;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: _theme.scaffoldBackgroundColor,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          centerTitle: true,
          title:  Text(
            "Available Trips",
            style: TextStyle(color: _theme.primaryColor),
          ),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              if (Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
            },
          ),
        ),
      body: Container(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                physics: const ClampingScrollPhysics(),
                itemBuilder: (context, pos) => _rideCard(availableTrips[pos]), itemCount: availableTrips.length,),
            ),
            _postCard(context)
          ],
        ),
      ),
      floatingActionButton: Visibility(
        visible: false,
        child: formButton(label: "Post My Trip", onPressed: () {
          Navigator.pushNamed(context, createTripPassengerRoute);
        }, borderRadius: 10.0, borderColor: _theme.primaryColor, color: _theme.primaryColor),
      ),
    );
  }

  _rideCard(Trip trip) {
    return Container(
      margin: const EdgeInsets.only(
        top: 10.0,
      ),
      child: Card(
        elevation: 0.0,
        child: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
               Text(
                trip.formatPickupDate(),
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pushNamed(context, rideDetails, arguments: {
                    "trip": trip,
                  });
                },
                contentPadding: const EdgeInsets.only(
                  left: 0.0,
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children:  <Widget>[
                    const Icon(
                      Icons.pin_drop,
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    Flexible(child:  Text(
                      "${trip.pickupAddress} - ${trip.dropOffAddress} ",
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    ))
                  ],
                ),
                trailing: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children:  <Widget>[
                    const Text(
                      "Price",
                      style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      "${formatMoney(trip.pricePerSeat)}",
                      style: const TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _postCard(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Container(
      margin: const EdgeInsets.only(
        top: 10.0,
      ),
      decoration: BoxDecoration(
          border: Border.all(color: _theme.primaryColor),
          borderRadius: BorderRadius.circular(10)
      ),
      child: Card(
        elevation: 0.0,
        child: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Couldn't find a ride ?".toUpperCase(),
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0,
                ),
              ),
              const SizedBox(height: 10,),
              ListTile(
                  onTap: () {
                  },
                  contentPadding: const EdgeInsets.only(
                    left: 0.0,
                  ),
                  subtitle: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      const Icon(
                        Icons.pin_drop,
                      ),
                      const SizedBox(
                        width: 5.0,
                      ),
                      Flexible(child: Text(
                        "${pickUpLocation?.description}  -  ${dropOffLocation?.description}.",
                        style: const TextStyle(
                          fontSize: 12
                        ),
                      ))
                    ],
                  ),
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                    flex: 1,
                    child: Text('Post ride if not in available rides', style: TextStyle(color: Colors.grey, fontSize: 12),),),
                  const SizedBox(width: 8,),
                  Expanded(flex: 1, child: formButton(label: "Post My Trip", onPressed: () {

                    Navigator.pushNamed(context, createTripPassengerRoute, arguments:  {
                      "pickUpLocation": pickUpLocation, "dropOffLocation": dropOffLocation
                    });

                  }, borderRadius: 10.0, horizontalPadding: 25.0, borderColor: _theme.primaryColor, color: _theme.primaryColor), )

                ],
              )
            ],
          ),
        ),
      ),
    );
  }



}