import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/utils.dart';

class BoatCruiseDetailsScreen extends StatefulWidget {
  const BoatCruiseDetailsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<BoatCruiseDetailsScreen> {

  final TextEditingController _seatsController = TextEditingController();

  int noOfSeats = 1;
  int pricePerSeat = 150000;
  int totalPrice = 150000;
  bool availableSeatLimitReached = false;

  @override
  void initState() {
    super.initState();
    _seatsController.text = noOfSeats.toString();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(150),
        child: AppBar(
          foregroundColor: Colors.black38,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              if (Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
            },
          ),
          flexibleSpace: Container(
            child: Stack(
              children: [
                Container(
                  foregroundDecoration: BoxDecoration(
                      color: Colors.black87.withOpacity(.8),
                      ),
                  decoration: const BoxDecoration(
                      color: Colors.black38,
                      image: DecorationImage(
                          image: AssetImage("assets/images/boat.jpeg"),
                          fit: BoxFit.fill)),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: const EdgeInsets.only(top: 50),
                    width: 200,
                    child: const Text(
                      "The Mv Kalanage experince ",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    )
                  ),
                )
              ],
            ),
          ),
          actions: [
            InkWell(
              onTap: () {},
              child: const Icon(
                Icons.favorite_border,
                color: Colors.white,
              ),
            ),
            const SizedBox(width: 10,)
          ],
        ),
      ),
      body: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            SizedBox(
              height: 60,
              child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: 15,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(
                      width: 10,
                    ),
                itemBuilder: (BuildContext context, int index) => Container(

                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.asset(
                      "assets/images/boat.jpeg",
                      height: 180.0,
                      width: 100.0,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20,),
            Column(
              children:[
                Card(
                  margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
                  child: Padding(
                    padding: EdgeInsets.all(25.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children:[
                        Row(
                          children: const [
                            Text('High Comfort', style: TextStyle(fontWeight: FontWeight.bold),),
                            SizedBox(width: 20,),
                            Text('UGX 150,000 per person',style: TextStyle(fontWeight: FontWeight.w300),),
                          ],
                        ),
                      ],
                    ),
                  ),

                ),
                SizedBox(height: 5,),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height *.3,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20),
                    ),
                    child: Align(
                      alignment: Alignment.bottomRight,
                      heightFactor: 0.3,
                      widthFactor: 2.5,
                      child: GoogleMap(
                        myLocationButtonEnabled: true,
                        myLocationEnabled: true,
                        zoomControlsEnabled: false,
                        initialCameraPosition: const CameraPosition(
                          target: LatLng(0.3247348, 32.5514714),
                          zoom: 11.0,
                        ),
                        onMapCreated: (GoogleMapController controller) async {
                        },
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10.0,),
                Stack(
                  children: [
                    Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height *.1,
                          child: Row(
                            children: [
                              FloatingActionButton(
                                onPressed: (){},
                                child: Icon(Icons.ac_unit_rounded),
                                backgroundColor: Colors.white,
                                shape: CircleBorder(
                                  side: BorderSide(
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              const SizedBox(width: 15,),
                              MaterialButton(
                                minWidth: 250,
                                height: 100,
                                color: Colors.black,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  side: BorderSide(
                                    color: Colors.black12,
                                  ),
                                ),
                                onPressed: (){
                                  _bookingDialogDetails();
                                },
                                child: const Text('GET TICKETS', style: TextStyle(color: Colors.white),),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),

          ],
        ),
      ),
    );
  }

  _bookingDialogDetails() {
    final ThemeData _theme = Theme.of(context);
    showModalBottomSheet(

        context: context,
        builder: (BuildContext bc) {
          return StatefulBuilder(
            builder: (context, state) {
              return Container(
                padding: const EdgeInsets.all(20),
                child: Wrap(
                  children: [
                    const Text('Confirm Booking', style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 18),
                      textAlign: TextAlign.center,),
                    Container(height: 20,),

                    Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Container(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: const [
                                      Text('3:48'),
                                      Text('Thur 1, Apr'),
                                    ],
                                  ),
                                ),
                                const Spacer(),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const Text('40 Tickets'),
                                      Text.rich(
                                        TextSpan(
                                          children: [
                                            TextSpan(
                                              text: 'UGX ${formatMoney(
                                                  pricePerSeat, currency: '')}',
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(text: ' per ticket',
                                                style: TextStyle(fontSize: 12)),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(height: 10,),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "Number of tickets",
                            style: TextStyle(),
                          ),
                          Spacer(),
                          Container(
                              padding: const EdgeInsets.all(5),
                              decoration: ShapeDecoration(
                                color: Colors.grey[200],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                              child: InkWell(
                                child: Icon(
                                  Icons.add,
                                ),
                                onTap: () {
                                  _increaseSeats(state);
                                },
                              )),
                          Container(
                            width: 35,
                            height: 35,
                            margin: const EdgeInsets.symmetric(horizontal: 10),
                            child: TextField(
                              controller: _seatsController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  filled: true,
                                  hintStyle: const TextStyle(
                                      color: Colors.grey),
                                  fillColor: Colors.white70),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.all(5),
                              decoration: ShapeDecoration(
                                color: Colors.grey[200],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                              child: InkWell(
                                child: Icon(
                                  Icons.remove,
                                ),
                                onTap: () {
                                  _decreaseSeats(state);
                                },
                              )),
                        ],
                      ),
                    ),
                    Container(height: 10,),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        children: [
                          const Text('Total Price'),
                          const SizedBox(width: 10,),
                          Text('UGX ${formatMoney(totalPrice, currency: '')}'),
                        ],
                      ),
                    ),
                    const SizedBox(height: 40,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FloatingActionButton(
                            backgroundColor: _theme.primaryColor,
                            child: const Icon(
                              Icons.check, color: Colors.white,),
                            onPressed: () {
                              //Navigator.pop(context);
                              aweSomeDialog(context: context,
                                  dialogType: DialogType.SUCCES,
                                  title: "Confirmed",
                                  desc: "Tickets have been booked.\n We shall contact you shortly.",
                                  btnOkPress: () {

                                    Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
                                  });
                            })
                      ],
                    )
                  ],
                ),
              );
            },
          );
        });
  }

  _increaseSeats(state) {
    modalSetState(state, () {
      if (noOfSeats < 40) {
        noOfSeats++;
        totalPrice = pricePerSeat * noOfSeats;
        _seatsController.text = noOfSeats.toString();
      } else {
        availableSeatLimitReached = true;
      }
    });
  }

  _decreaseSeats(state) {
    modalSetState(state, () {
      if (noOfSeats > 1) {
        noOfSeats--;
        totalPrice = pricePerSeat * noOfSeats;
        _seatsController.text = noOfSeats.toString();
        availableSeatLimitReached = false;
      }
    });
  }
}
