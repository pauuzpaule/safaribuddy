import 'package:flutter/material.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';

class BoatCruiseListScreen extends StatefulWidget {
  const BoatCruiseListScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<BoatCruiseListScreen> {
  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "Boat Cruise",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        padding:const EdgeInsets.all(10),
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: 15,
          separatorBuilder: (BuildContext context, int index) =>
              const SizedBox(
                height: 10,
              ),
          itemBuilder: (BuildContext context, int index) => InkWell(
            onTap: () {
              Navigator.pushNamed(context, boatCruiseDetailsRoute);
            },
            child: Card(
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              child: Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Container(
                        height: 100.0,
                        width: 100.0,
                        child: Image.asset(
                          "assets/images/boat2.png",
                          height: 100.0,
                          width: 100.0,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10,),
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:  [
                          const Text("Today 12:30 PM", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                          const SizedBox(height: 3,),
                          Row(
                            children: const [
                              Icon(Icons.location_on, size: 18, color: Colors.black87,),
                              SizedBox(width: 5,),
                              Flexible(child: Text("Kalangala sesse - Gaba beach")),
                            ],
                          ),
                          const SizedBox(height: 3,),
                          Row(
                            children: const [
                              Icon(Icons.directions_boat, size: 18, color: Colors.black87,),
                              SizedBox(width: 5,),
                              Flexible(child: Text("MV Kalangala")),
                            ],
                          ),
                          const SizedBox(height: 10,),
                          Row(
                            children: const [
                              Icon(Icons.people, size: 18, color: Colors.black87,),
                              SizedBox(width: 3,),
                              Text("24", style: TextStyle(fontSize: 12),),
                              Spacer(),
                              Text("UGX 150,000", style: TextStyle(fontWeight: FontWeight.bold),)
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: formButton(
          label: "Post boat cruise",
          verticalPadding: 15.0,
          borderRadius: 10.0,
          borderColor: _theme.primaryColor,
          color: _theme.primaryColor,
          onPressed: () {
            Navigator.pushNamed(context, addBoatCruiseRoute);
          }),
    );
  }
}
