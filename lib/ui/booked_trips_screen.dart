import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/data/booked_trip.dart';
import 'package:safaribuddy/data/trip.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/viewModel/trip_view_model.dart';

class BookedTripsScreen extends StatefulWidget {
  const BookedTripsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<BookedTripsScreen>  with TickerProviderStateMixin{

  List<BookedTrip> bookedTrips = [];
  bool isPassenger = true;

  TabController? _tabController;


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      SharedPrefs().getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });

      _tabController =  TabController(length: 3, vsync: this);
      _tabController!.addListener(_handleTabSelection);


      getBookedTrips();
    });
  }

  _handleTabSelection() {
    if (_tabController!.indexIsChanging) {
      int index = _tabController!.index;
      var statuses = ['pending', 'accepted', 'rejected'];
      getBookedTrips(status: statuses[index]);

    }
  }


  getBookedTrips({status = 'pending'}) {
    TripViewModel tripViewModel = Provider.of<TripViewModel>(context, listen: false);
    tripViewModel.getBookedTrips(context: context, status: status).whenComplete(() {
      setState(() {
        bookedTrips = tripViewModel.bookedTrips;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    TripViewModel tripViewModel = Provider.of<TripViewModel>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "My Trips",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(15.0),
        child: tripViewModel.isLoading ? loadingView() : Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(
              height: 15.0,
            ),
            Expanded(
              child: DefaultTabController(
                length: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TabBar(
                      controller: _tabController,
                      unselectedLabelColor: Colors.grey,
                      labelColor: _theme.primaryColor,
                      labelStyle: const TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      ),
                      indicatorColor: Colors.black,
                      tabs: const <Widget>[
                        Tab(
                          text: "PENDING",
                        ),
                        Tab(
                          text: "ACCEPTED",
                        ),
                        Tab(
                          text: "REJECTED",
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                   Container(
                     child: tripViewModel.isLoading ? loadingView() :  _contentView(),
                   )

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _contentView() {
    return bookedTrips.isEmpty ? Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height *.3),
      child:  Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Icon(Icons.sentiment_neutral_rounded),
            Text('No Data')
          ],
        ),
      ),
    ) : Expanded(
      child: ListView.builder(
        shrinkWrap: true,
        physics: const ClampingScrollPhysics(),
        itemBuilder: (context, pos) => _rideCard(bookedTrips[pos]), itemCount: bookedTrips.length,),
    );
  }

  _rideCard(BookedTrip bookedTrip) {
    Trip trip = bookedTrip.trip;
    return Container(
      margin: const EdgeInsets.only(
        top: 10.0,
      ),
      child: Card(
        elevation: 0.0,
        child: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                trip.formatPickupDate(),
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pushNamed(context, rideDetails, arguments: {
                    "trip": trip,
                  });
                },
                contentPadding: const EdgeInsets.only(
                  left: 0.0,
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children:  <Widget>[
                    const Icon(
                      Icons.pin_drop,
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    Flexible(child:  Text(
                      "${trip.pickupAddress} - ${trip.dropOffAddress} ",
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    ))
                  ],
                ),
                trailing: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children:  <Widget>[
                    const Text(
                      "Price",
                      style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      "${formatMoney(trip.pricePerSeat)}",
                      style: const TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}