import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:characters/characters.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/utils.dart';

class CarHireDetailsScreen extends StatefulWidget {
  const CarHireDetailsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<CarHireDetailsScreen> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  final List<String> imgList = [
    'https://vimg.cheki.co.ug/xl/1_inventory1860053_1549052620.jpg',
    'https://vimg.cheki.co.ug/xl/1_inventory1860053_1549052625.jpg',
    'https://vimg.cheki.co.ug/xl/1_inventory1860053_1549052588.jpg',
    'https://vimg.cheki.co.ug/xl/1_inventory1860053_1549052602.jpg'
  ];

  final TextEditingController _seatsController = TextEditingController();

  int noOfSeats = 1;
  int pricePerSeat = 50000;
  int totalPrice = 50000;
  bool availableSeatLimitReached = false;

  @override
  void initState() {
    super.initState();
    _seatsController.text = noOfSeats.toString();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    final screenHeight = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: screenHeight * .4,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    title: indicators(),
                    background: Stack(
                      children: [
                        CarouselSlider(
                          items: imageSliders(),
                          carouselController: _controller,
                          options: CarouselOptions(
                              height: screenHeight * 4,
                              autoPlay: true,
                              viewportFraction: 1,
                              aspectRatio: 2.0,
                              onPageChanged: (index, reason) {
                                setState(() {
                                  _current = index;
                                });
                              }),
                        )
                      ],
                    )),
              ),
            ];
          },
          body: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                ListTile(
                  title: Text("Model"),
                  subtitle: Text('Mercedese Benz ML'),
                ),
                ListTile(
                  title: Text("License Plate"),
                  subtitle: Text('UBA 200X'),
                ),
                ListTile(
                  title: Text("Price"),
                  subtitle: Text('UGX 150,000 per day'),
                ),
                ListTile(
                  title: Text("Description"),
                  subtitle: Text(kDummyText.characters.take(100).string),
                ),
                const Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        padding: const EdgeInsets.all(10),
                        decoration: ShapeDecoration(
                          color: Colors.grey[300],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: const Icon(
                            Icons.call,
                          ),
                          onTap: () {},
                        )),
                    const SizedBox(
                      width: 10,
                    ),
                    Container(
                        padding: const EdgeInsets.all(10),
                        decoration: ShapeDecoration(
                          color: Colors.grey[300],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: const Icon(
                            Icons.chat,
                          ),
                          onTap: () {},
                        )),
                    const SizedBox(
                      width: 10,
                    ),
                    formButton(
                        label: "Book now",
                        borderRadius: 10,
                        onPressed: () {
                          _bookingDialogDetails();
                        },
                        borderColor: _theme.primaryColor,
                        color: _theme.primaryColor,
                        verticalPadding: 15,
                        horizontalPadding: 50),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget indicators() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: imgList.asMap().entries.map((entry) {
        return GestureDetector(
          onTap: () => _controller.animateToPage(entry.key),
          child: Container(
            width: 6.0,
            height: 6.0,
            margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: (Theme.of(context).brightness == Brightness.dark
                        ? Colors.grey
                        : Colors.white)
                    .withOpacity(_current == entry.key ? 0.9 : 0.4)),
          ),
        );
      }).toList(),
    );
  }

  List<Widget> imageSliders() {
    final screenHeight = MediaQuery.of(context).size.height;
    return imgList
        .map((item) => Container(
              child: Container(
                child: ClipRRect(
                    borderRadius: BorderRadius.all(const Radius.circular(5.0)),
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Image.network(item, fit: BoxFit.fill),
                        Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromARGB(200, 0, 0, 0),
                                  Color.fromARGB(0, 0, 0, 0)
                                ],
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
            ))
        .toList();
  }

  _bookingDialogDetails() {
    final ThemeData _theme = Theme.of(context);
    showModalBottomSheet(

        context: context,
        builder: (BuildContext bc) {
          return StatefulBuilder(
            builder: (context, state) {
              return Container(
                padding: const EdgeInsets.all(20),
                child: Wrap(
                  children: [
                    const Text('Confirm Booking', style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 18),
                      textAlign: TextAlign.center,),
                    Container(height: 20,),

                    Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Container(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: const [
                                      Text('3:48'),
                                      Text('Thur 1, Apr'),
                                    ],
                                  ),
                                ),
                                const Spacer(),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const Text('Self Drive'),
                                      Text.rich(
                                        TextSpan(
                                          children: [
                                            TextSpan(
                                              text: 'UGX ${formatMoney(
                                                  pricePerSeat, currency: '')}',
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(text: ' per day',
                                                style: TextStyle(fontSize: 12)),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(height: 10,),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "Number of days",
                            style: TextStyle(),
                          ),
                          Spacer(),
                          Container(
                              padding: const EdgeInsets.all(5),
                              decoration: ShapeDecoration(
                                color: Colors.grey[200],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                              child: InkWell(
                                child: Icon(
                                  Icons.add,
                                ),
                                onTap: () {
                                  _increaseSeats(state);
                                },
                              )),
                          Container(
                            width: 35,
                            height: 35,
                            margin: const EdgeInsets.symmetric(horizontal: 10),
                            child: TextField(
                              controller: _seatsController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  filled: true,
                                  hintStyle: const TextStyle(
                                      color: Colors.grey),
                                  fillColor: Colors.white70),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.all(5),
                              decoration: ShapeDecoration(
                                color: Colors.grey[200],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                              child: InkWell(
                                child: Icon(
                                  Icons.remove,
                                ),
                                onTap: () {
                                  _decreaseSeats(state);
                                },
                              )),
                        ],
                      ),
                    ),
                    Container(height: 10,),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        children: [
                          const Text('Total Price'),
                          const SizedBox(width: 10,),
                          Text('UGX ${formatMoney(totalPrice, currency: '')}'),
                        ],
                      ),
                    ),
                    const SizedBox(height: 40,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FloatingActionButton(
                            backgroundColor: _theme.primaryColor,
                            child: const Icon(
                              Icons.check, color: Colors.white,),
                            onPressed: () {
                              //Navigator.pop(context);
                              aweSomeDialog(context: context,
                                  dialogType: DialogType.SUCCES,
                                  title: "Confirmed",
                                  desc: "Booking details received.\n We shall contact you shortly.",
                                  btnOkPress: () {

                                    Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
                                  });
                            })
                      ],
                    )
                  ],
                ),
              );
            },
          );
        });
  }

  _increaseSeats(state) {
    modalSetState(state, () {
      if (noOfSeats < 40) {
        noOfSeats++;
        totalPrice = pricePerSeat * noOfSeats;
        _seatsController.text = noOfSeats.toString();
      } else {
        availableSeatLimitReached = true;
      }
    });
  }

  _decreaseSeats(state) {
    modalSetState(state, () {
      if (noOfSeats > 1) {
        noOfSeats--;
        totalPrice = pricePerSeat * noOfSeats;
        _seatsController.text = noOfSeats.toString();
        availableSeatLimitReached = false;
      }
    });
  }
}
