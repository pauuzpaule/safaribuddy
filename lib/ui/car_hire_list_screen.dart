import 'package:flutter/material.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';

class CarHireListScreen extends StatefulWidget {
  const CarHireListScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<CarHireListScreen> {
  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "Available Cars",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        padding:const EdgeInsets.all(10),
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: 15,
          separatorBuilder: (BuildContext context, int index) =>
          const SizedBox(
            height: 10,
          ),
          itemBuilder: (BuildContext context, int index) => InkWell(
            onTap: () {
              Navigator.pushNamed(context, carHireDetailsRoute);
            },
            child: Card(
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              child: Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Container(
                        color: Colors.black45,
                        height: 100.0,
                        width: 100.0,
                        child: Image.asset(
                          "assets/images/car_small.png",
                          height: 100.0,
                          width: 100.0,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10,),
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:  const [
                          Text("UAB 200 L", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                          SizedBox(height: 3,),
                          Flexible(child: Text("Mercedes Benz ML")),
                          SizedBox(height: 3,),
                          Flexible(child: Text("Self Drive/ Designated Driver")),
                          SizedBox(height: 10,),
                          Text("UGX 150,000 per Day", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 11),)
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: formButton(
          label: "Post Car for Hire",
          verticalPadding: 15.0,
          borderRadius: 10.0,
          borderColor: _theme.primaryColor,
          color: _theme.primaryColor,
          onPressed: () {
            Navigator.pushNamed(context, addCarForHireRoute);
          }),
    );
  }
}
