import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/util/color_utils.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/utils.dart';

class OtpVerificationScreen extends StatefulWidget {
  const OtpVerificationScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<OtpVerificationScreen> {

  final _formKey = GlobalKey<FormState>();
  String otp = '', viewMessage = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _initView(),
    );
  }

  _initView() {
    return Container(
      padding: const EdgeInsets.all(25),
      child: Center(
        child: _formView(),
      ),
    );
  }

  _formView() {

    try{
      var intentData = ModalRoute.of(context)!.settings.arguments as Map;
      if(intentData['message'] != null) {
        viewMessage = intentData['message'];
      }
    }catch(e) {}

    return Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          clipBehavior: Clip.none,
          children: [
            const SizedBox(height: 30,),
            Text(viewMessage),
            PinCodeTextField(
              appContext: context,
              length: 6,
              animationType: AnimationType.fade,
              pinTheme: PinTheme(
                  shape: PinCodeFieldShape.underline,
                  borderRadius: BorderRadius.circular(5),
                  fieldHeight: 50,
                  fieldWidth: 40,
                  activeFillColor: Colors.white,
                  activeColor: primaryColor(),
                  inactiveColor: primaryColor()
              ),
              animationDuration: const Duration(milliseconds: 300),
              enableActiveFill: false,
              onCompleted: (v) {
                setState(() {
                  otp = v;
                });
                print("Completed");
              },
              onChanged: (value) {
              },
              beforeTextPaste: (text) {
                print("Allowing to paste $text");
                return true;
              },
            ),

            const SizedBox(height: 20,),
            Container(
              child: formButton(label: "Verify", verticalPadding: 15.0, onPressed: () async {
                hideKeyBoard(context);
                if(otp.length == 6) {
                  Navigator.of(context).pop(otp);
                }
              }),),

          ],
        ));
  }

}