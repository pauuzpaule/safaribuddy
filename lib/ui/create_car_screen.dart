import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/viewModel/car_view_model.dart';
import 'package:safaribuddy/widgets/form_widgets.dart';

class CreateCarScreen extends StatefulWidget {
  const CreateCarScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<CreateCarScreen> {
  double imageWidth = 100;
  double imageHeight = 80;
  bool isLoading = false;

  final picker = ImagePicker();
  final List<File> _carPics = [];

  TextEditingController _nameController = TextEditingController();
  TextEditingController _modelController = TextEditingController();
  TextEditingController _platesController = TextEditingController();
  TextEditingController _seatsController = TextEditingController();

  final _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, "Create Car"),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: creationForm(),
      ),
    );
  }

  creationForm() {
    CarViewModel carViewModel = Provider.of<CarViewModel>(context, listen: false);
    return Form(
      key: _formKey,
      child: ListView(
        children: [
          const SizedBox(
            height: 5,
          ),
          textFieldInputRounded(hintText: "Name", controller: _nameController,
            validator: (text) {
              if (text == null || text.isEmpty) {
                return 'Name is required';
              }
              return null;
            },),
          const SizedBox(
            height: 10,
          ),
          textFieldInputRounded(hintText: "Model", controller: _modelController,
            validator: (text) {
              if (text == null || text.isEmpty) {
                return 'Model is required';
              }
              return null;
            },),
          const SizedBox(
            height: 10,
          ),
          textFieldInputRounded(hintText: "License Plate", controller: _platesController,
            validator: (text) {
              if (text == null || text.isEmpty) {
                return 'License Plat is required';
              }
              return null;
            },),
          const SizedBox(
            height: 10,
          ),
          textFieldInputRounded(hintText: "Number of Seats", keyboardType: TextInputType.number, controller: _seatsController,
            validator: (text) {
              if (text == null || text.isEmpty) {
                return 'Field is required';
              }
              return null;
            },),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            height: imageHeight,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buttonAddImage(),
                const SizedBox(
                  width: 10,
                ),
                Flexible(
                    child: ListView.separated(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: _carPics.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(
                    width: 10,
                  ),
                  itemBuilder: (BuildContext context, int index) =>
                      carItemAddImage(_carPics[index], index),
                ))
              ],
            ),
          ),
          const SizedBox(height: 30),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: isLoading ?  loadingView() : formButton(
                label: "Save",
                verticalPadding: 20,
                borderRadius: 10,
                onPressed: () {
                  if(_formKey.currentState!.validate()) {
                    if(_carPics.isEmpty) {
                      showToast("Add at least one image of the car");
                      return;
                    }else {

                      setState(() {
                        isLoading = true;
                      });
                      var request = {
                        'name': _nameController.text,
                        'model': _modelController.text,
                        'licensePlate': _platesController.text,
                        'numberOfSeats': _seatsController.text,
                      };

                      carViewModel.createCar(request, context: context).whenComplete(() async {
                        if(carViewModel.carRegisteredSuccessfully) {
                          await uploadCarImages(carViewModel.registeredCar!.id);

                          setState(() {
                            isLoading = false;
                          });
                        }else {
                          String error = carViewModel.carRegistrationError;
                          if(error != kInternetError) {
                            showSnackBar(
                                context, error,
                                seconds: 5,
                                action: SnackBarAction(
                                    label: "Ok",
                                    onPressed: () async {}));
                          }
                        }
                      });

                    }
                  }
                }),
          )
        ],
      ),
    );
  }


  uploadCarImages(carId) async {

    CarViewModel carViewModel = Provider.of<CarViewModel>(context, listen: false);

    await Future.forEach(_carPics, (File element) async {
      await carViewModel.uploadCarImage(carId.toString(), element, context: context);
    });

    showToast("Car registerd successfully");

  }

  buttonAddImage() {
    return InkWell(
      onTap: () {
        if(_carPics.length >= 3) {
          showToast("Only 3 images allowed");
        }else {
          _imagePickerModel(context);
        }
      },
      child: Container(
        width: imageWidth,
        height: imageHeight,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: Colors.grey)),
        child: const Center(
          child: Icon(Icons.add_a_photo_outlined),
        ),
      ),
    );
  }

  carItemAddImage(File file, int position) {
    return Container(
      width: imageWidth,
      height: imageHeight,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: Colors.grey)),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.file(
              file,
              fit: BoxFit.fill,
              height: imageHeight,
              width: imageWidth,
            ),
          ),
          Positioned(
            right: 0,
            top: -2,
            child: InkWell(
              onTap: () {
                setState(() {
                  _carPics.removeAt(position);
                });
              },
              child: const Icon(Icons.cancel),
            ),
          )
        ],
      ),
    );
  }

  Future<void> _cropImage(File imageFile) async {
    File? croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
              ]
            : [
                CropAspectRatioPreset.square,
              ],
        androidUiSettings: const AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.square,
            lockAspectRatio: false),
        iosUiSettings: const IOSUiSettings(
          title: 'Cropper',
        ));
    if (croppedFile != null) {
     // _image = croppedFile;
    setState(() {
      _carPics.add(croppedFile);
    });

    }
  }

  void _imagePickerModel(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return StatefulBuilder(builder: (context, state) {
            return Wrap(
              children: <Widget>[
                ListTile(
                    leading: const Icon(Icons.picture_in_picture),
                    title: const Text('Gallery'),
                    onTap: () async {
                      final pickedFile = await (picker.pickImage(
                        source: ImageSource.gallery,
                        maxWidth: 1800,
                        maxHeight: 1800,
                      ));

                      if(pickedFile != null) {
                        _cropImage(File(pickedFile.path))
                            .then(((value) => Navigator.pop(context)));
                      }

                    }),
                ListTile(
                  leading: const Icon(Icons.camera),
                  title: const Text('Camera'),
                  onTap: () async {
                    final pickedFile = await (picker.pickImage(
                      source: ImageSource.camera,
                      maxWidth: 1800,
                      maxHeight: 1800,
                    ));

                    if(pickedFile != null) {
                      _cropImage(File(pickedFile.path))
                          .then(((value) => Navigator.pop(context)));
                    }
                  },
                ),
              ],
            );
          });
        }).then((value) async {

    });
  }
}
