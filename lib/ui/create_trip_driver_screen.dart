import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/widgets/dash.dart';

class CreateTripDriverScreen extends StatefulWidget {
  const CreateTripDriverScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<CreateTripDriverScreen> {
  String tripType = kPassengerTrip;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Stack(
            children: [
              GoogleMap(
                myLocationButtonEnabled: false,
                myLocationEnabled: true,
                zoomControlsEnabled: false,
                initialCameraPosition: const CameraPosition(
                  target: LatLng(0.3247348, 32.5514714),
                  zoom: 16.0,
                ),
                onMapCreated: (GoogleMapController controller) async {},
              ),
              Positioned.fill(top: 100, child: _chooseTripView())
            ],
          ),
        ),
      ),
    );
  }

  _chooseTripView() {
    final ThemeData _theme = Theme.of(context);
    return Container(
      decoration:  BoxDecoration(
          color: _theme.primaryColor,
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30))),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                const Text(
                  "Create Trip",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                const Spacer(),
                InkWell(
                  child: const Icon(Icons.close, color: Colors.white),
                  onTap: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, homeRoute, (route) => false);
                  },
                )
              ],
            ),
          ),
          Expanded(
              child: Container(
            padding: const EdgeInsets.all(20),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40))),
            child: ListView(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: const [
                    Text(
                      "Trip",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                _locationView(),
                const SizedBox(
                  height: 15,
                ),
                Divider(
                  color: Colors.grey[200],
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "Type",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                Row(
                  children: [
                    Radio(
                      fillColor: MaterialStateColor.resolveWith(
                          (states) => _theme.primaryColor),
                      value: kPassengerTrip,
                      groupValue: tripType,
                      onChanged: (value) {
                        setState(() {
                          tripType = value as String;
                        });
                      },
                      activeColor: Colors.green,
                    ),
                    const Text("Passenger"),
                  ],
                ),
                Row(
                  children: [
                    Radio(
                      fillColor: MaterialStateColor.resolveWith(
                          (states) => _theme.primaryColor),
                      value: kCargoTrip,
                      groupValue: tripType,
                      onChanged: (value) {
                        setState(() {
                          tripType = value as String;
                        });
                      },
                      activeColor: Colors.green,
                    ),
                    const Text("Cargo"),
                  ],
                ),
                Divider(
                  color: Colors.grey[200],
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "Seat and Time",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Container(
                      height: 8,
                      width: 8,
                      decoration:  BoxDecoration(
                          color: _theme.primaryColor, shape: BoxShape.circle),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text(
                      "Number of Seats",
                      style: TextStyle(fontSize: 16),
                    ),
                    const Spacer(),
                    Container(
                        padding: EdgeInsets.all(5),
                        decoration: ShapeDecoration(
                          color: Colors.grey[200],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: Icon(
                            Icons.add,
                          ),
                          onTap: () {},
                        )),
                    Container(
                      width: 35,
                      height: 35,
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: TextField(
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            filled: true,
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "1",
                            fillColor: Colors.white70),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.all(5),
                        decoration: ShapeDecoration(
                          color: Colors.grey[200],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: Icon(
                            Icons.remove,
                          ),
                          onTap: () {},
                        )),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Container(
                      height: 8,
                      width: 8,
                      decoration:  BoxDecoration(
                          color: _theme.primaryColor, shape: BoxShape.circle),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text(
                      "Schedule Time",
                      style: TextStyle(fontSize: 16),
                    ),
                    const Spacer(),
                    Container(
                        padding: const EdgeInsets.all(5),
                        decoration: ShapeDecoration(
                          color: Colors.grey[200],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: const Icon(
                            Icons.access_time_outlined,
                          ),
                          onTap: () {},
                        )),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                Divider(
                  color: Colors.grey[200],
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "Select Car",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 150,
                  child: ListView.separated(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: 15,
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(
                      width: 10,
                    ),
                    itemBuilder: (BuildContext context, int index) => Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: Image.asset(
                                "assets/images/car_small.png",
                                height: 100.0,
                                width: 100.0,
                              ),
                            ),
                            Text(
                              "Model T UAB 700F",
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Container(
                        padding: const EdgeInsets.all(16),
                        decoration: ShapeDecoration(
                          color: Colors.grey[200],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: const Icon(
                            Icons.arrow_back,
                          ),
                          onTap: () {
                            Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
                          },
                        )),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: formButton(
                            verticalPadding: 20.0,
                            borderRadius: 10.0,
                            textSize: 16,
                            borderColor: _theme.primaryColor,
                            color: _theme.primaryColor,
                            label: "Post Trip",
                            onPressed: () {
                              Navigator.pushNamed(context, availableRidesRoute);
                            }))
                  ],
                )
              ],
            ),
          ))
        ],
      ),
    );
  }

  _locationView() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 15,
              width: 15,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 5, color: Colors.black)),
            ),
            const SizedBox(
              width: 20,
            ),
            InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text("Pick up",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.w700)),
                  SizedBox(
                    height: 3,
                  ),
                  Text("kololo Airstrip, Kampala", style: TextStyle()),
                ],
              ),
              onTap: () {},
            )
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.only(left: 7),
              child: Dash(
                  direction: Axis.vertical,
                  length: 35,
                  dashLength: 1,
                  dashGap: 0,
                  dashColor: Colors.black),
            ),
            const SizedBox(
              width: 15,
            ),
            Expanded(
              child: Divider(
                height: 3,
                color: Colors.grey[200],
              ),
            )
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 15,
              width: 15,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 5, color: Colors.black)),
              child: Container(
                height: 20,
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text("Drop off",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.w700)),
                  SizedBox(
                    height: 3,
                  ),
                  Text("Makerere Rd, Kampala", style: TextStyle()),
                ],
              ),
              onTap: () {},
            )
          ],
        )
      ],
    );
  }
}
