import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/data/picked_place.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/viewModel/trip_view_model.dart';
import 'package:safaribuddy/widgets/dash.dart';

class CreateTripPassengerScreen extends StatefulWidget {
  const CreateTripPassengerScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<CreateTripPassengerScreen> {
  PickedPlace? pickUpLocation, dropOffLocation;
  int numberOfSeats = 1;
  final TextEditingController _numberOfSeatsEd = TextEditingController();
  String dateTime = "";

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      _numberOfSeatsEd.text = numberOfSeats.toString();
      try {
        var intentData =
            ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
        setState(() {
          if (intentData.containsKey("pickUpLocation")) {
            pickUpLocation = intentData["pickUpLocation"];
          }
          if (intentData.containsKey("dropOffLocation")) {
            dropOffLocation = intentData["dropOffLocation"];
          }
        });
      } catch (e, trace) {
        print(e);
        print(trace);
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Stack(
            children: [
              GoogleMap(
                myLocationButtonEnabled: false,
                myLocationEnabled: true,
                zoomControlsEnabled: false,
                initialCameraPosition: const CameraPosition(
                  target: LatLng(0.3247348, 32.5514714),
                  zoom: 16.0,
                ),
                onMapCreated: (GoogleMapController controller) async {},
              ),
              Positioned.fill(top: 100, child: _chooseTripView())
            ],
          ),
        ),
      ),
    );
  }

  _chooseDateAndTime() {
    datePicker(context, (date) {
      setState(() {
        dateTime = date;
      });

      timePicker(context, (time) {
        setState(() {
          dateTime = "$dateTime $time";
        });
      });
    });
  }

  _chooseTripView() {
    final ThemeData _theme = Theme.of(context);
    TripViewModel tripViewModel =
        Provider.of<TripViewModel>(context, listen: false);
    return Container(
      decoration: BoxDecoration(
          color: _theme.primaryColor,
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30))),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              children: [
                const Text(
                  "Create Trip",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                const Spacer(),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: const Icon(Icons.close, color: Colors.white),
                )
              ],
            ),
          ),
          Expanded(
              child: Container(
            padding: const EdgeInsets.all(20),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    const Text(
                      "Trip",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    const Spacer(),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        "Change",
                        style: TextStyle(color: Colors.blue),
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                _locationView(),
                const SizedBox(
                  height: 15,
                ),
                Divider(
                  color: Colors.grey[200],
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "Seat and Time",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Container(
                      height: 8,
                      width: 8,
                      decoration: const BoxDecoration(
                          color: Colors.black, shape: BoxShape.circle),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text(
                      "Number of Seats",
                      style: TextStyle(fontSize: 16),
                    ),
                    const Spacer(),
                    Container(
                        padding: const EdgeInsets.all(5),
                        decoration: ShapeDecoration(
                          color: Colors.grey[200],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: const Icon(
                            Icons.add,
                          ),
                          onTap: () {
                            setState(() {
                              numberOfSeats++;
                              _numberOfSeatsEd.text = numberOfSeats.toString();
                            });
                          },
                        )),
                    Container(
                      width: 45,
                      height: 35,
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        readOnly: true,
                        controller: _numberOfSeatsEd,
                        decoration: InputDecoration(
                            contentPadding:
                                const EdgeInsets.only(left: 10, top: 18),
                            //Change this value to custom as you like
                            isDense: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            filled: true,
                            hintStyle: const TextStyle(color: Colors.grey),
                            hintText: "1",
                            fillColor: Colors.white70),
                      ),
                    ),
                    Container(
                        padding: const EdgeInsets.all(5),
                        decoration: ShapeDecoration(
                          color: Colors.grey[200],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: const Icon(
                            Icons.remove,
                          ),
                          onTap: () {
                            if (numberOfSeats > 1) {
                              setState(() {
                                numberOfSeats--;
                                _numberOfSeatsEd.text =
                                    numberOfSeats.toString();
                              });
                            }
                          },
                        )),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Container(
                      height: 8,
                      width: 8,
                      decoration: const BoxDecoration(
                          color: Colors.black, shape: BoxShape.circle),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text(
                      "Schedule Time",
                      style: TextStyle(fontSize: 16),
                    ),
                    const Spacer(),
                    Text(
                      dateTime,
                      style: const TextStyle(color: Colors.blue),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Container(
                        padding: const EdgeInsets.all(5),
                        decoration: ShapeDecoration(
                          color: Colors.grey[200],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: const Icon(
                            Icons.access_time_outlined,
                          ),
                          onTap: () {
                            _chooseDateAndTime();
                          },
                        )),
                  ],
                ),
                const Spacer(),
                Container(
                  child: tripViewModel.isLoading
                      ? loadingView()
                      : Row(
                          children: [
                            Container(
                                padding: const EdgeInsets.all(16),
                                decoration: ShapeDecoration(
                                  color: Colors.grey[200],
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                                child: InkWell(
                                  child: Icon(
                                    Icons.arrow_back,
                                  ),
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                )),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                                child: formButton(
                                    verticalPadding: 20.0,
                                    borderRadius: 10.0,
                                    textSize: 16,
                                    borderColor: _theme.primaryColor,
                                    color: _theme.primaryColor,
                                    label: "Post Trip",
                                    onPressed: () {


                                      if(dateTime.isEmpty) {
                                        showToast("Time is required.");
                                        return;
                                      }

                                      var request = {
                                        "type": "passenger",
                                        "numberOfSeats":
                                            _numberOfSeatsEd.text,
                                        "pickupTime":
                                            "${dateFromLocalToUtc(dateTime)}",
                                        "pickUpCoordinates":
                                            "${pickUpLocation?.lat}, ${pickUpLocation?.lng}",
                                        "pickUpAddress":
                                            "${pickUpLocation?.description}",
                                        "dropOffCoordinates":
                                            "${dropOffLocation?.lat}, ${dropOffLocation?.lng}",
                                        "dropOffAddress":
                                            "${dropOffLocation?.description}"
                                      };

                                      tripViewModel
                                          .createTrip(request, context: context)
                                          .whenComplete(() {
                                        if (tripViewModel
                                            .tripCreatedSuccessfully!) {
                                          aweSomeDialog(
                                              context: context,
                                              dialogType: DialogType.SUCCES,
                                              title: "",
                                              desc:
                                                  "Trip has been posted.\n We shall contact you shortly.",
                                              btnOkPress: () {
                                                Navigator
                                                    .pushNamed(
                                                        context,
                                                        myRides, arguments: {
                                                          "gotoHome": true
                                                });
                                              });
                                        } else {
                                          String error =
                                              tripViewModel.tripCreationError!;
                                          if (error != kInternetError) {
                                            showSnackBar(context, error,
                                                seconds: 5,
                                                action: SnackBarAction(
                                                    label: "Ok",
                                                    onPressed: () async {}));
                                          }
                                        }
                                      });

                                      print(jsonEncode(request));

                                      //Navigator.pushNamed(context, availableRidesRoute);
                                    }))
                          ],
                        ),
                )
              ],
            ),
          ))
        ],
      ),
    );
  }

  _locationView() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 15,
              width: 15,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 5, color: Colors.black)),
            ),
            const SizedBox(
              width: 20,
            ),
            InkWell(
              child: SizedBox(
                width: MediaQuery.of(context).size.width*0.7,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("Pick up",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 14,
                            fontWeight: FontWeight.w700)),
                    const SizedBox(
                      height: 3,
                    ),
                    Text("${pickUpLocation?.description}",
                        style: const TextStyle()),
                  ],
                ),
              ),
              onTap: () {},
            )
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.only(left: 7),
              child: Dash(
                  direction: Axis.vertical,
                  length: 35,
                  dashLength: 1,
                  dashGap: 0,
                  dashColor: Colors.black),
            ),
            const SizedBox(
              width: 15,
            ),
            Expanded(
              child: Divider(
                height: 3,
                color: Colors.grey[200],
              ),
            )
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 15,
              width: 15,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 5, color: Colors.black)),
              child: Container(
                height: 20,
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text("Drop off",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.w700)),
                  const SizedBox(
                    height: 3,
                  ),
                  Text("${dropOffLocation?.description}", style: TextStyle()),
                ],
              ),
              onTap: () {},
            )
          ],
        )
      ],
    );
  }
}
