import 'dart:math';

import 'package:flutter/material.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/widgets/ride_card.dart';
import 'package:safaribuddy/widgets/ride_cards.dart';

class DriverRidesScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<DriverRidesScreen> {

  bool isPassenger = true;


  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      SharedPrefs().getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "Trips",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(
              height: 15.0,
            ),
            Expanded(
              child: DefaultTabController(
                length: 4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TabBar(
                      unselectedLabelColor: Colors.grey,
                      labelColor: _theme.primaryColor,
                      labelStyle: const TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      ),
                      indicatorColor: Colors.black,
                      isScrollable: true,
                      tabs: const <Widget>[
                        Tab(
                          text: "BOOKINGS",
                        ),
                        Tab(
                          text: "UPCOMING",
                        ),
                        Tab(
                          text: "COMPLETED",
                        ),
                        Tab(
                          text: "CANCELED",
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Expanded(
                      child: TabBarView(
                        children: <Widget>[
                          _bookings(),
                          SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                RideCard(),
                                RideCard(),
                                RideCard(),
                                RideCard(),
                                RideCard(),
                                RideCard(),
                                RideCard(),
                              ],
                            ),
                          ),
                          Container(
                            child: RideCard(),
                          ),
                          Container(
                            child: RideCard(),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _bookings() {
    return ListView.separated(itemBuilder: (context, pos) => _bookingItem(), separatorBuilder: (context, pod) => const SizedBox(height: 10,), itemCount: 3);
  }

  _bookingItem() {
    final ThemeData _theme = Theme.of(context);
    int bookings = Random().nextInt(4) + 1;
    return Container(
      margin: const EdgeInsets.only(
        top: 10.0,
      ),
      child: Card(
        elevation: 0.0,
        child: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "Today, 10:30 AM",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17.0,
                    ),
                  ),
                  Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.black87
                    ),
                    child: Center(
                      child: Text('$bookings', style: TextStyle(color: Colors.white),),
                    ),
                  )
                ],
              ),
              SizedBox(height: 10,),
              ListTile(
                onTap: () {
                    Navigator.pushNamed(context, tripRequestRoute, arguments: bookings);
                },
                contentPadding: const EdgeInsets.only(
                  left: 0.0,
                ),
                title: const Text(
                  "TOYOTA CAMRY",
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const <Widget>[
                    Icon(
                      Icons.pin_drop,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      "Home - Kawempe Rd.",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
                trailing: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const <Widget>[
                    Text(
                      "Price",
                      style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      "\$45.00",
                      style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
