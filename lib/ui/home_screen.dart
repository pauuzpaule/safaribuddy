import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/data/car.dart';
import 'package:safaribuddy/data/recent_destination.dart';
import 'package:safaribuddy/data/trip.dart';
import 'package:safaribuddy/util/color_utils.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/widgets/app_drawer.dart';
import 'package:safaribuddy/widgets/dash.dart';
import 'package:safaribuddy/widgets/form_widgets.dart';


// TODO delete class
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<HomeScreen> {
  Position? position;
  CameraPosition? _currentCameraPosition;
  final Completer<GoogleMapController> _controller = Completer();

  String setDate = "", setTime = "", tripType = kPassengerTrip;
  bool isPassenger = true;


  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      SharedPrefs().getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });
    });
    super.initState();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;


    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        print("Errror xxxxx---------------> Location permissions are permanently denied, we cannot request permissions.");
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      print("Errror---------------> Location permissions are permanently denied, we cannot request permissions.");
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: initView(),
      drawer: AppDrawer(),
    ));
  }

  initView() {
    return Container(
      child: Stack(
        fit: StackFit.expand,
        children: [
           GoogleMap(
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            initialCameraPosition: const CameraPosition(
              target: LatLng(0.3247348, 32.5514714),
              zoom: 11.0,
            ),
            onMapCreated: (GoogleMapController controller) async {
              _controller.complete(controller);


              await _determinePosition().then((value) {
                CameraPosition camPosition = CameraPosition(target: LatLng(value.latitude, value.longitude), zoom: 15);
                setState(() {
                  _currentCameraPosition = camPosition;
                });
                controller.moveCamera(CameraUpdate.newCameraPosition(camPosition));

              });

            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: const EdgeInsets.all(8),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  appButton(
                      label: "Where are you going?",
                      onPressed: () {
                        showSearchAreaBottomSheet();
                      })
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 5),
              child: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    icon: const Icon(
                      Icons.menu,
                      size: 35.0,
                    ),
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    },
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }


  showSearchAreaBottomSheet() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (BuildContext context) {
        return SafeArea(
            child: DraggableScrollableSheet(
                expand: false,
                initialChildSize: .4,
                maxChildSize: 1,
                minChildSize: 0.25,
                builder: (context, scrollController) {
                  return SingleChildScrollView(
                    controller: scrollController,
                    child: Container(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).viewInsets.bottom,
                          left: 10,
                          right: 10,
                          top: 10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: 5,
                                width: 65,
                                decoration: const BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(40.0),
                                      bottomRight: Radius.circular(40.0),
                                      topLeft: Radius.circular(40.0),
                                      bottomLeft: Radius.circular(40.0)),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Expanded(child: textFieldInput(
                                  onTap: () {

                                  },
                                  hintText: "Search for destination")),
                              InkWell(onTap: () {
                                Navigator.pushNamed(context, pickLocationRoute).then((value) {
                                  Navigator.of(context).pop();
                                  if(value != null) {
                                    value = value as Map;
                                    onDestinationPickedDialog();
                                  }
                                });
                              }, child: const Icon(Icons.search))
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 18),
                            child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, pickLocationRoute).then((value) {
                                  Navigator.of(context).pop();
                                  if(value != null) {
                                    value = value as Map;
                                    onDestinationPickedDialog();
                                  }
                                });
                              },
                              child: Row(
                                children: [
                                  iconWithBg(const Icon(Icons.location_on), color: Colors.blue[100]),
                                  const SizedBox(width: 10,),
                                  const Text('Choose on map')
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                              child: recentDestinations(context))
                        ],
                      ),
                    ),
                  );
                }));
      },
    );
  }

  recentDestinations(cxt) {
    List<RecentDestination> _list = RecentDestination().dummyData().take(3).toList();
    return ListView.separated(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        RecentDestination item = _list[index];
        return ListTile(
          onTap: () {
            Navigator.of(cxt).pop();
            onDestinationPickedDialog();
          },
          dense: true,
          leading:iconWithBg(const Icon(Icons.access_time), color: Colors.grey[200]) ,
          title: Text('${item.address}'),
          subtitle: Text('${item.district}'),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return const Divider();
      },  itemCount: _list.length,);
  }

  onDestinationPickedDialog() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (BuildContext context) {
        return Container(
          padding: const EdgeInsets.all(20),
          child: Wrap(
            children: [
              bottomSheetDash(),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [Text(
                  "Select pick up point",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                ),],
              ),
              Container(height: 15,),
              Container(
                padding: const  EdgeInsets.symmetric(horizontal: 10),
                child: Row(children:  [
                  const Icon(
                    Icons.location_on,
                    color: Colors.green,
                  ),
                  const SizedBox(width: 10,),
                  const Flexible(child: Text("Kawempe Tula Rd", style: TextStyle(),),),
                  const Spacer(),
                  InkWell(onTap: () {
                    Navigator.of(context).pop();
                    Navigator.pushNamed(context, pickLocationRoute).then((value) {
                      if(value != null) {
                        value = value as Map;
                        print(value["lat"]);
                        onDestinationPickedDialog();
                      }
                    });
                  }, child: const Icon(Icons.search, size: 18,))
                ]),
              ),
              Container(height: 30,),
              appButton(label: "Confirm Pick up", onPressed: () {
                Navigator.of(context).pop();
                showStartTripBottomSheet();
              })

            ],
          ),
        );
      },
    );
  }

  showStartTripBottomSheet() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (BuildContext context) {
        return Container(
          padding: const EdgeInsets.all(20),
          child: Wrap(
            children: [
              bottomSheetDash(),
              const SizedBox(height: 15,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text(
                    "Trip",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              const SizedBox(height: 35,),
              SizedBox(
                height: 70,
                child: Stack(
                  children: [
                    Positioned(
                      top: 15,
                      left: 11,
                      child: Dash(
                          direction: Axis.vertical,
                          length: 35,
                          dashLength: 2,
                          dashColor: Colors.grey),),
                    Positioned(
                      top: 0,
                      left: 0,
                      child: Row(children: const [
                        Icon(
                          Icons.location_on,
                          color: Colors.green,
                        ),
                        SizedBox(width: 10,),
                        Text("Kawempe", style: TextStyle(fontWeight: FontWeight.w600),)
                      ]),
                    ),
                    Positioned(
                      top: 35,
                      left: 0,
                      child: Row(children: const [
                        Icon(
                          Icons.location_on,
                          color: Colors.red,
                        ),
                        SizedBox(width: 10,),
                        Text("Dfcu bank wandegeya", style: TextStyle(fontWeight: FontWeight.w600))
                      ]),
                    ),

                  ],
                ),
              ),
              const Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Visibility(
                    visible: isPassenger,
                    child: formButton(
                        label: "Available tips",
                        borderRadius: 15,
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.pushNamed(context, availableRidesRoute);
                        },
                        verticalPadding: 2,
                        horizontalPadding: 10),
                  ),
                  const SizedBox(width: 10,),
                  formButton(
                      label: "Schedule Trip",
                      borderRadius: 15,
                      onPressed: () async {

                        Navigator.of(context).pop();
                        if(isPassenger) {
                          scheduleTripPassenger();
                        }else {
                          scheduleTrip();
                        }

                      },
                      color: Colors.blueAccent,
                      borderColor: Colors.blueAccent,
                      verticalPadding: 2,
                      horizontalPadding: 10),
                  const SizedBox(width: 10,),
                  formButton(
                      label: "Cancel",
                      borderRadius: 15,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      color: Colors.red,
                      borderColor: Colors.red,
                      verticalPadding: 2,
                      horizontalPadding: 10)
                ],
              ),

            ],
          ),
        );
      },
    );
  }

  scheduleTripPassenger() {
    setState(() {
      setDate = "";
      setTime = "";
    });
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, _state) {
          return SingleChildScrollView(
            child: Container(
              padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom, left: 20, right: 20, top: 20),
              child: Column(
                children: [
                  bottomSheetDash(),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        "Schedule trip",
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton.icon(onPressed: () {
                        datePicker(context, (date) {
                          modalSetState(_state, () {
                            setDate = date;
                          });
                        });
                      }, icon: const Icon(Icons.calendar_today_outlined), label:  Text(setDate.isNotEmpty ? setDate : "Set date")),
                      TextButton.icon(onPressed: () {
                        timePicker(context, (time) {
                          modalSetState(_state, () {
                            setTime = time;
                          });
                        });
                      }, icon: const Icon(Icons.access_time_outlined), label:  Text (setTime.isNotEmpty ? setTime :"Set time")),
                    ],
                  ),
                  Container(height: 20,),
                  appButton(label: "Confirm", onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.pushNamed(context, myRides);
                  }),
                  const SizedBox(height: 20,)

                ],
              ),
            ),
          );
        });
      },
    );
  }

  scheduleTrip() {
    setState(() {
      setDate = "";
      setTime = "";
    });
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, _state) {
          return SingleChildScrollView(
            child: Container(
              padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom, left: 20, right: 20, top: 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 5,
                        width: 65,
                        decoration: const BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(40.0),
                              bottomRight: Radius.circular(40.0),
                              topLeft: Radius.circular(40.0),
                              bottomLeft: Radius.circular(40.0)),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Schedule trip",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(height: 35,),
                  Row(
                    children: [
                      Radio(
                        value: kPassengerTrip,
                        groupValue: tripType,
                        onChanged: (value) {
                            modalSetState(_state, () {
                              tripType = value as String;
                            });
                        },
                        activeColor: Colors.green,
                      ),
                      const Text("Passenger"),
                    ],
                  ),
                  Row(
                    children: [
                      Radio(
                        value: kCargoTrip,
                        groupValue: tripType,
                        onChanged: (value) {
                          modalSetState(_state, () {
                            tripType = value as String;
                          });
                        },
                        activeColor: Colors.green,
                      ),
                      const Text("Cargo"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton.icon(onPressed: () {
                        datePicker(context, (date) {
                          modalSetState(_state, () {
                            setDate = date;
                          });
                        });
                      }, icon: const Icon(Icons.calendar_today_outlined), label:  Text(setDate.isNotEmpty ? setDate : "Set date")),
                      TextButton.icon(onPressed: () {
                        timePicker(context, (time) {
                          modalSetState(_state, () {
                            setTime = time;
                          });
                        });
                      }, icon: const Icon(Icons.access_time_outlined), label:  Text (setTime.isNotEmpty ? setTime :"Set time")),
                    ],
                  ),

                  textFieldInput(hintText: "Price per seat", keyboardType: const TextInputType.numberWithOptions()),
                  textFieldInput(hintText: "Number of seats", keyboardType: const TextInputType.numberWithOptions()),
                  textFieldInput(hintText: "Car license plate"),
                  Container(height: 20,),
                  appButton(label: "Confirm", onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.pushNamed(context, myRides);
                  }),
                  const SizedBox(height: 20,)

                ],
              ),
            ),
          );
        });
      },
    );
  }
}
