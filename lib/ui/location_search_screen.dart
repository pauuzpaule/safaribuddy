import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_place/google_place.dart';
import 'package:safaribuddy/data/picked_place.dart';
import 'package:safaribuddy/data/place_search.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/shared_prefs.dart';

class LocationSearchScreen extends StatefulWidget {
  const LocationSearchScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<LocationSearchScreen> {


  final SharedPrefs sharedPreferences = SharedPrefs();
  List<PlaceSearch> placeResults = [];

  var googlePlace = GooglePlace("AIzaSyDTdO8x0Fsm2NkcKKvoBrmQ2qQ9-mASTxk");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light.copyWith(
          statusBarColor: Colors.grey,
        ),
        child: SafeArea(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      filled: true,
                      prefixIcon: InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: const Icon(Icons.arrow_back),
                      ),
                      hintStyle: TextStyle(color: Colors.grey[800]),
                      hintText: "Search location",
                      fillColor: Colors.white70,
                  ),
                  onChanged: (value) async {
                    if(value.isNotEmpty && value.length >=3) {
                        searchPlace(value);
                    }else {
                      setState(() {
                        placeResults = [];
                      });
                    }

                  },
                ),
                const SizedBox(height: 30,),
                Container(
                  padding: const EdgeInsets.only(left: 18),
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Row(
                      children: [
                        iconWithBg(const Icon(Icons.my_location), color: Colors.blue[100]),
                        const SizedBox(width: 20,),
                        const Text('Your location')
                      ],
                    ),
                  ),
                ),
                const Divider(),
                Container(
                  padding: const EdgeInsets.only(left: 18),
                  child: InkWell(
                    onTap: () async {
                      //Navigator.of(context).pop();
                      List<PlaceSearch> list = await sharedPreferences.getCachedPlaces();
                      PlaceSearch place = PlaceSearch(description: "Masaka, Uganda", placeId: "ChIJ0wz6EJeO1xkRDkvRQzoPNz8");
                      
                      if(list.map((e) => e.placeId).contains(place.placeId)) {
                        print("PLace.............");
                      }
                      list.forEach((element) {
                        print(element.toJson());
                      });

                    },
                    child: Row(
                      children: [
                        iconWithBg(const Icon(Icons.location_on), color: Colors.grey[300]),
                        const SizedBox(width: 20,),
                        const Text('Choose on map')
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                    child: ListView.builder(
                        itemCount: placeResults.length,
                        itemBuilder: (context, index) => ListTile(
                          onTap: () async {
                            var result = await googlePlace.details.get(placeResults[index].placeId);
                            if (result != null && result.result != null) {
                              print(result.result!.geometry!.location);
                              print(result.result!.addressComponents![0].shortName);

                              Location? location = result.result!.geometry!.location;
                              var description = result.result!.addressComponents![0].longName;

                              if(location != null) {
                                Navigator.of(context).pop(PickedPlace(lat: location.lat, lng: location.lng, description: placeResults[index].description));
                              }


                            }
                          },
                          leading: const Icon(Icons.location_on),
                          title: Text(placeResults[index].description),
                        )),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  searchPlace(String value) async {
    List<PlaceSearch> list = await sharedPreferences.getCachedPlaces();
    List<PlaceSearch> resList = [];

    List<PlaceSearch> searchList = list.where((element) => element.description.toLowerCase().contains(value.toLowerCase())).toList();


    if(searchList.isNotEmpty) {
      print("Local search --------------------------> ");
        setState(() {
          searchList.sort((a, b) => a.description.compareTo(b.description));
          placeResults = searchList;
        });
    }else {
      var result = await googlePlace.autocomplete.get(value, components: [Component("country", "ug")]);
      if (result != null && result.predictions != null) {
        print("Api search --------------------------> ");
        result.predictions!.forEach((element) {
          final item = PlaceSearch(description: element.description.toString(), placeId: element.placeId.toString(),);
          resList.add(item);
          if(!list.map((e) => e.placeId).contains(item.placeId)) {
            list.add(item);
          }
        });
        sharedPreferences.storeCachedPlaces(list);
        setState(() {
          resList.sort((a, b) => a.description.compareTo(b.description));
          placeResults = resList;
        });
      }
    }

  }

}