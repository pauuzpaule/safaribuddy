import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/viewModel/auth_view_model.dart';
import 'package:safaribuddy/widgets/form_widgets.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<LoginScreen> {

  TextEditingController _phoneEditingController = TextEditingController();
  TextEditingController _passwordEditingController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    super.dispose();
    _phoneEditingController.dispose();
    _passwordEditingController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(registerRoute);
            },
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Text(
                "Sign Up",
                style: TextStyle(
                  color: _theme.primaryColor,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 20.0),
                  child: Text(
                    "Log In",
                    style: _theme.textTheme.headline5!.merge(
                      const TextStyle(fontSize: 30.0),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30.0,
                ),
                _loginForm(context),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _loginForm(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    var authViewModel = Provider.of<AuthViewModel>(context);
    return Form(
      key: _formKey,
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        textFieldInput(
          controller: _phoneEditingController,
          hintText: "Phone",
          keyboardType: TextInputType.phone,
          validator: (text) {
            if (text == null || text.isEmpty) {
              return 'Phone number is required';
            } else if (formatPhoneNumberWithCode(text) == null) {
              return 'Enter valid phone number';
            }
            return null;
          },
        ),
        const SizedBox(
          height: 20.0,
        ),
        textFieldInput(
          controller: _passwordEditingController,
          hintText: "Password",
          obscureText: true,
          validator: (text) {
            if (text == null || text.isEmpty) {
              return 'Password is required';
            }
            return null;
          },
        ),
        const SizedBox(
          height: 20.0,
        ),
        Text(
          "Forgot password?",
          style: TextStyle(
              color: _theme.primaryColor,
              fontSize: 16.0,
              fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 25.0,
        ),
        Container(
          child: authViewModel.isLoading ? loadingView() : appButton(label: "LOG IN", onPressed: () {
            if(_formKey.currentState!.validate()) {
              var request = {
                "phoneNumber": formatPhoneNumberWithCode(_phoneEditingController.text),
                "password": _passwordEditingController.text
              };

              authViewModel.loginUser(request, context: context).whenComplete(() {
                if(authViewModel.loginSuccessful!) {
                  Navigator.of(context).pushNamedAndRemoveUntil(homeRoute, (route) => false);
                }else {
                  String error = authViewModel.loginErrorMessage;
                  if(error != kInternetError) {
                    showSnackBar(
                        context, error,
                        seconds: 5,
                        action: SnackBarAction(
                            label: "Ok",
                            onPressed: () async {}));
                  }
                }
              });

            }
          }),
        )
      ],
    ));
  }
}
