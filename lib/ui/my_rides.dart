import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/data/trip.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/viewModel/trip_view_model.dart';
import 'package:safaribuddy/widgets/ride_card.dart';

class MyRidesScreen extends StatefulWidget {
  const MyRidesScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<MyRidesScreen> {

  bool isPassenger = true;
  List<Trip> availableTrips = [];
  bool gotoHomeOnBackPress = false;


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      SharedPrefs().getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });


      try{
        var intentData = ModalRoute.of(context)!.settings.arguments as Map;
        if(intentData.containsKey("gotToHome")) {
          setState(() {
            gotoHomeOnBackPress = true;
          });
        }
      }catch(e) {}

      getTrips();
    });
  }


  getTrips() {
    TripViewModel tripViewModel = Provider.of<TripViewModel>(context, listen: false);
    tripViewModel.getUserTrips(context: context).whenComplete(() {
      setState(() {
        availableTrips = tripViewModel.tripList;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    TripViewModel tripViewModel = Provider.of<TripViewModel>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "My Trips",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              if(gotoHomeOnBackPress) {
                Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
              }else {
                Navigator.of(context).pop();
              }

            }
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(15.0),
        child: tripViewModel.isLoading ? loadingView() : Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(
              height: 15.0,
            ),
            Expanded(
              child: DefaultTabController(
                length: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TabBar(
                      unselectedLabelColor: Colors.grey,
                      labelColor: _theme.primaryColor,
                      labelStyle: const TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      ),
                      indicatorColor: Colors.black,
                      tabs: const <Widget>[
                        Tab(
                          text: "UPCOMING",
                        ),
                        Tab(
                          text: "COMPLETED",
                        ),
                        Tab(
                          text: "CANCELED",
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Expanded(
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: const ClampingScrollPhysics(),
                        itemBuilder: (context, pos) => _rideCard(availableTrips[pos]), itemCount: availableTrips.length,),
                    ),

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _rideCard(Trip trip) {
    return Container(
      margin: const EdgeInsets.only(
        top: 10.0,
      ),
      child: Card(
        elevation: 0.0,
        child: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                trip.formatPickupDate(format: 'yyyy-MM-ddTHH:mm:ss.000Z'),
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pushNamed(context, rideDetails, arguments: {
                    "trip": trip,
                    "ownTrip":  true,
                  });
                },
                contentPadding: const EdgeInsets.only(
                  left: 0.0,
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children:  <Widget>[
                    const Icon(
                      Icons.pin_drop,
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    Flexible(child:  Text(
                      "${trip.pickupAddress} - ${trip.dropOffAddress} ",
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    ))
                  ],
                ),
                trailing: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:  <Widget>[
                    Text(
                      "${trip.tripBids!.length}",
                      style: const TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
