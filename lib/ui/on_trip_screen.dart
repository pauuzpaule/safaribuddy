import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/util/color_utils.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/widgets/form_widgets.dart';

class OnTripScreen extends StatefulWidget {
  const OnTripScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<OnTripScreen> {

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: initView()
        ));
  }

  initView() {
    return Container(
      child: Stack(
        fit: StackFit.expand,
        children: [
          const GoogleMap(
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            initialCameraPosition: CameraPosition(
              target: LatLng(0.3247348, 32.5514714),
              zoom: 11.0,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: const EdgeInsets.all(8),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  appButton(
                      label: "End Trip",
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              margin: const EdgeInsets.only(top: 80),
              child:  Text("4 min", style: TextStyle(color: primaryColor(), fontWeight: FontWeight.bold)),
              decoration:  BoxDecoration(
                color: Colors.green[200],
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0)),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 50),
              margin: const EdgeInsets.only(top: 20),
              child:  Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text("Drop off", style: TextStyle(color: Colors.white, fontSize: 16)),
                  Text("Dfcu bank, wandegeya", style: TextStyle(color: Colors.white))
                ],
              ),
              decoration:  const BoxDecoration(
                color: Colors.black45,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0)),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 10, top: 10),
              child: backButton(context: context),
            ),
          )
        ],
      ),
    );
  }

}