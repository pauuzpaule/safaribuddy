import 'package:flutter/material.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';

class PackageDetailsScreen extends StatefulWidget {
  const PackageDetailsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<PackageDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "Delivery Details",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        padding:const EdgeInsets.all(10),
        child: Column(
          children:  [
            ListTile(
              title: Text('Date', style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text('Sat, 12/Nov/2021'),
            ),
            ListTile(
              title: Text('Recipient Name', style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text('Paul Sserwanja'),
            ),
            ListTile(
              title: Text('Recipient Phone Number', style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text('(256) 752000000'),
            ),
            ListTile(
              title: Text('Description', style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text(kDummyText.characters.take(80).string),
            ),
            ListTile(
              title: Text('Route', style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text('Kawempe, Kampala - kasana Luwero'),
            ),
            ListTile(
              title: Text('Driver', style: TextStyle(fontWeight: FontWeight.bold),),
              trailing: Wrap(
                children: [
                  Icon(Icons.star_border, size: 17,),
                  Text('4.6')
                ],
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 5,),
                  Text('Mugalu David'),
                  Text('(256) 704000000'),
                ],
              )
            )
          ],
        ),
      ),
    );
  }
}
