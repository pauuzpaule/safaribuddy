import 'package:flutter/material.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';

class PackagesListScreen extends StatefulWidget {
  const PackagesListScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<PackagesListScreen> {
  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "My Packages",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        padding:const EdgeInsets.all(10),
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: 15,
          separatorBuilder: (BuildContext context, int index) =>
          const SizedBox(
            height: 10,
          ),
          itemBuilder: (BuildContext context, int index) => InkWell(
            onTap: () {
              Navigator.pushNamed(context, packageDetailsRoute);
            },
            child: Card(
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              child: Container(
                padding: const EdgeInsets.all(10),
                child: ListTile(
                  title: Text("Sat, 12/Nov/21"),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text('Sserwanja Paul'),
                      Text('(256) 752000000'),
                      Text('Kawempe, Kampala - kasana Luwero'),
                    ],
                  ),
                  trailing: tagLabel(Text("Delivered", style: TextStyle(color: Colors.white, fontSize: 10),), color: Colors.green),
                ),
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: formButton(
          label: "Send Package",
          verticalPadding: 15.0,
          borderRadius: 10.0,
          borderColor: _theme.primaryColor,
          color: _theme.primaryColor,
          onPressed: () {
            Navigator.pushNamed(context, postPackageDeliveryRoute);
          }),
    );
  }
}
