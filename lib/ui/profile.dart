import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:safaribuddy/data/auth_obj.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/widgets/form_widgets.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
   return _State();
  }

}

class _State extends State<ProfileScreen> {

  bool isPassenger = true;
  SharedPrefs sharedPreferences = SharedPrefs();
  User? currentUser;

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      sharedPreferences.getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });

      sharedPreferences.getAuthObj().then((value) {
        if(value != null) {
          currentUser = value.user;
        }
      });


    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        title: const Text("Profile", style: TextStyle(color: Colors.black),),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "${currentUser?.fullName()} !",
                    style:
                        _theme.textTheme.headline5!.merge(const TextStyle(fontSize: 26.0)),
                  ),
                  const CircleAvatar(
                    radius: 25.0,
                    backgroundImage: AssetImage("assets/images/person.png"),
                  )
                ],
              ),
              const SizedBox(
                height: 25.0,
              ),
              textFieldInput(
                hintText: "Name",
                initialValue: currentUser?.fullName(),
              ),
              const SizedBox(
                height: 15.0,
              ),
              textFieldInput(
                hintText: "Email",
                initialValue: "",
                suffixIcon: Icon(
                  Icons.check_circle,
                  color: _theme.primaryColor,
                ),
              ),
              const SizedBox(
                height: 15.0,
              ),
              textFieldInput(
                hintText: "Phone Number",
                initialValue: currentUser?.phoneNumber,
              ),
              const SizedBox(
                height: 15.0,
              ),
              const Text(
                "PREFERENCES",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 14.0,
                  color: Color(0xFF9CA4AA),
                ),
              ),
              const SizedBox(
                height: 10.0,
              ),
              Container(
                decoration: BoxDecoration(
                  color: const Color(0xffFBFBFD),
                  border: Border.all(
                    color: const Color(0xffD6D6D6),
                  ),
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 10.0,
                  vertical: 20.0,
                ),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        const Expanded(
                          child: Text(
                            "RECEIVE RECEIPT MAILS",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Switch(
                          value: true,
                          activeColor: _theme.primaryColor,
                          onChanged: (bool state) {},
                        )
                      ],
                    ),
                    const Text(
                      "The switch is the widget used to achieve the popular.",
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
              ),

              const SizedBox(
                height: 10.0,
              ),
              Container(
                decoration: BoxDecoration(
                  color: const Color(0xffFBFBFD),
                  border: Border.all(
                    color: const Color(0xffD6D6D6),
                  ),
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 10.0,
                  vertical: 20.0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        const Expanded(
                          child: Text(
                            "SWITCH USER TYPE",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        FlutterSwitch(
                          width: 125.0,
                          height: 32.0,
                          valueFontSize: 12.0,
                          toggleSize: 15.0,
                          activeText: "Passenger",
                          inactiveText: "Driver",
                          inactiveToggleColor: Colors.white,
                          inactiveColor: Colors.green,
                          value: isPassenger,
                          borderRadius: 30.0,
                          padding: 8.0,
                          showOnOff: true,
                          onToggle: (val) async {
                            setState(() {
                              isPassenger = val;
                            });

                            if(isPassenger) {

                              await SharedPrefs().saveUserType(kPassenger);

                            }else {
                              await SharedPrefs().saveUserType(kDriver);
                            }

                            Phoenix.rebirth(context);
                          },
                        )
                      ],
                    ),
                    const Text(
                      "Select Preferred user type",
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
              ),

              const SizedBox(height: 24.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  formButton(label: "Logout", onPressed: () async {
                    await SharedPrefs().removeAllData();
                    Navigator.pushNamedAndRemoveUntil(context, loginRoute, (route) => false);
                  }, borderRadius: 15)
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
