import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/styles/colors.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/viewModel/auth_view_model.dart';
import 'package:safaribuddy/widgets/form_widgets.dart';
import 'package:country_picker/country_picker.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<RegisterScreen> {
  String countryCode = "256";
  String phoneNumber = "";
  TextEditingController _countryEditingController = TextEditingController();
  TextEditingController _firstnameController = TextEditingController();
  TextEditingController _lastnameEditingController = TextEditingController();
  TextEditingController _phoneEditingController = TextEditingController();
  TextEditingController _passwordEditingController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  bool _passwordVisible = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      _countryEditingController.text = countryCode;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _countryEditingController.dispose();
    _firstnameController.dispose();
    _lastnameEditingController.dispose();
    _phoneEditingController.dispose();
    _passwordEditingController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    var authViewModel = Provider.of<AuthViewModel>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(loginRoute);
            },
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Text(
                "Log In",
                style: TextStyle(
                  color: _theme.primaryColor,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height - 100.0,
          padding: const EdgeInsets.all(15.0),
          margin: const EdgeInsets.only(bottom: 300.0),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.all(20),
                      child: Text(
                        "Sign Up",
                        style: _theme.textTheme.headline5!.merge(
                          const TextStyle(fontSize: 30.0),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    _signupForm(),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: authViewModel.isLoading
                          ? loadingView()
                          : appButton(
                              label: "SIGN UP",
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  var request = {
                                    "firstName": _firstnameController.text,
                                    "lastName": _lastnameEditingController.text,
                                    "phoneNumber": formatPhoneNumberWithCode(
                                        _phoneEditingController.text),
                                    "password": _passwordEditingController.text,
                                  };

                                  authViewModel
                                      .registerUser(request, context: context)
                                      .whenComplete(() {
                                    if (authViewModel.signUpSuccessful!) {
                                        verifyOtp(authViewModel);
                                    } else {
                                      String error = authViewModel.signUpError!;
                                      if(error != kInternetError) {
                                        showSnackBar(
                                            context, error,
                                            seconds: 5,
                                            action: SnackBarAction(
                                            label: "Ok",
                                            onPressed: () async {}));
                                      }

                                    }
                                  });
                                }
                              }),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  verifyOtp(AuthViewModel authViewModel,) {
    Navigator.of(context).pushNamed(
        otpVerificationRoute,
        arguments: {
          'message':
          "We sent a verification otp to ${authViewModel.user!.phoneNumber}."
        }).then((otp) {

      if(otp != null) {

        var otpRequest = {
          "phoneNumber": authViewModel.user!.phoneNumber,
          "otp": otp
        };

        authViewModel.verifyOtp(otpRequest, context: context).whenComplete(() {
          if(authViewModel.otpVerificationSuccessful!) {
            aweSomeDialog(context: context,
                dialogType: DialogType.SUCCES,
                title: "",
                desc: "Account verified successfully.",
                btnOkPress: () {
                  Navigator.pushNamedAndRemoveUntil(context, loginRoute, (route) => false);
                });
          }else {
            String error = authViewModel.verifyOtpErrorMessage!;
            if(error != kInternetError) {
              showSnackBar(
                  context, error,
                  seconds: 5,
                  action: SnackBarAction(
                      label: "Ok",
                      onPressed: () async {}));
            }
          }
        });
    }

    });
  }

  Widget _signupForm() {
    return Form(
        key: _formKey,
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: textFieldInput(
                      controller: _firstnameController,
                      hintText: "First name",
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'First name is required';
                        }
                        return null;
                      },
                    ),
                  ),
                  const SizedBox(width: 15.0),
                  Expanded(
                    child: textFieldInput(
                      controller: _lastnameEditingController,
                      hintText: "Last name",
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'Last name is required';
                        }
                        return null;
                      },
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 80.0,
                    child: textFieldInput(
                        readOnly: true,
                        controller: _countryEditingController,
                        hintText: "Country Code",
                        onTap: () {
                          showCountryPicker(
                            context: context,
                            showPhoneCode: true,
                            // optional. Shows phone code before the country name.
                            onSelect: (Country country) {
                              _countryEditingController.text =
                                  country.phoneCode;
                            },
                          );
                        }),
                  ),
                  const SizedBox(width: 15.0),
                  Expanded(
                    child: textFieldInput(
                      controller: _phoneEditingController,
                      hintText: "Phone number",
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'Phone number is required';
                        } else if (formatPhoneNumberWithCode(text) == null) {
                          return 'Enter valid phone number';
                        }
                        return null;
                      },
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),
              textFieldInput(
                controller: _passwordEditingController,
                hintText: "Password",
                obscureText: !_passwordVisible,
                suffixIcon: IconButton(
                  icon: Icon(
                    // Based on passwordVisible state choose the icon
                    !_passwordVisible ? Icons.visibility_off : Icons.visibility,
                    color: dbasicDarkColor,
                  ),
                  onPressed: () {
                    // Update the state i.e. toogle the state of passwordVisible variable
                    setState(() {
                      _passwordVisible = !_passwordVisible;
                    });
                  },
                ),
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Password is required';
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 25.0,
              ),
              Text(
                "By clicking \"Sign Up\" you agree to our terms and conditions as well as our pricacy policy",
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: dbasicDarkColor),
              )
            ],
          ),
        ));
  }
}
