import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';

class RideDetailsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<RideDetailsScreen> {
  final Completer<GoogleMapController> _controller = Completer();

  bool isPassenger = true;


  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      SharedPrefs().getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        title: const Text(
          "Ride details",
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        child: initView(),
      ),
    );
  }

  initView() {
    final ThemeData _theme = Theme.of(context);
    return Container(
      child: Column(
        children: [
          Expanded(
            child: GoogleMap(
              myLocationButtonEnabled: true,
              myLocationEnabled: true,
              initialCameraPosition: const CameraPosition(
                target: LatLng(0.3247348, 32.5514714),
                zoom: 11.0,
              ),
              onMapCreated: (GoogleMapController controller) async {
                _controller.complete(controller);
              },
            ),
          ),
          Container(
            padding: const EdgeInsets.all(20),
            child: Wrap(
              children: [
                const Text(
                  "Today, 10:30 AM",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17.0,
                  ),
                ),
                ListTile(
                  contentPadding: const EdgeInsets.only(
                    left: 0.0,
                  ),
                  title: const Text(
                    "TOYOTA CAMRY",
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  subtitle: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        width: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.pin_drop,
                            color: _theme.primaryColor,
                          ),
                          const SizedBox(
                            width: 5.0,
                          ),
                          const Text(
                            "Home - Kawempe Rd.",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 10,),
                      Text("Passenger Trip", style: TextStyle(fontWeight: FontWeight.bold),),
                    ],
                  ),
                  trailing: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      const Text(
                        "Price",
                        style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        "\$45.00",
                        style: TextStyle(
                          fontSize: 24.0,
                          color: _theme.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ),
                const Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Visibility(
                      visible: isPassenger,
                      child:  Container(
                        margin: const EdgeInsets.only(right: 10),
                        child: formButton(
                            label: "Call Driver",
                            borderRadius: 15,
                            onPressed: () {
                              if(isPassenger) {

                              }else {
                                Navigator.pushNamed(context, onTripRoute);
                              }
                            },
                            color: Colors.blueAccent,
                            borderColor: Colors.blueAccent,
                            verticalPadding: 2,
                            horizontalPadding: 10),
                      ),),
                    formButton(
                        label: isPassenger ? "Join ride" : "Start trip",
                        borderRadius: 15,
                        onPressed: () {
                          if(isPassenger) {

                          }else {
                            Navigator.pushNamed(context, onTripRoute);
                          }
                        },
                        verticalPadding: 2,
                        horizontalPadding: 10),
                    const SizedBox(width: 10,),
                    Visibility(
                      visible: isPassenger,
                      child:  Container(
                        margin: const EdgeInsets.only(right: 10),
                        child:  formButton(
                            label: "Chat",
                            borderRadius: 15,
                            onPressed: () {
                              Navigator.pushNamed(context, chatDetailsRoute);
                            },
                            color: Colors.red,
                            borderColor: Colors.red,
                            verticalPadding: 2,
                            horizontalPadding: 10),
                      ),),
                    Visibility(
                      visible: !isPassenger,
                      child:  Container(
                        margin: const EdgeInsets.only(right: 10),
                        child:  formButton(
                            label: "View requests",
                            borderRadius: 15,
                            onPressed: () {},
                            verticalPadding: 2,
                            horizontalPadding: 10),
                      ),),
                    Visibility(
                      visible: !isPassenger,
                        child: formButton(
                        label: "Delete trip",
                        borderRadius: 15,
                        onPressed: () {},
                        color: Colors.red,
                        borderColor: Colors.red,
                        verticalPadding: 2,
                        horizontalPadding: 10))
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
