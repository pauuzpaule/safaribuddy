import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/data/picked_place.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/widgets/dash.dart';

class ScheduleTripLocationScreen extends StatefulWidget {
  const ScheduleTripLocationScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<ScheduleTripLocationScreen> {

  String? pickUp, dropOff;
  PickedPlace? pickUpPickedPlace, dropOffPickedPlace;

  bool isPassenger = true;


  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      SharedPrefs().getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      body: SafeArea(
          child: Container(
        child: Stack(
          children: [
            GoogleMap(
              myLocationButtonEnabled: false,
              myLocationEnabled: true,
              zoomControlsEnabled: false,
              initialCameraPosition: const CameraPosition(
                target: LatLng(0.3247348, 32.5514714),
                zoom: 16.0,
              ),
              onMapCreated: (GoogleMapController controller) async {


              },
            ),
            Positioned(
              top: 10, left: 10,
                child: InkWell(
              child: Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)
                ),
                child: Container(
                  padding: const EdgeInsets.all(10),
                  child: const Icon(Icons.close),
                ),
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            )),

            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(top: 80, left: 40, right: 40),
                child: _locationPickView(),
              ),
            ),

            Positioned(
              bottom: 0,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: formButton(
                  minWidth: MediaQuery.of(context).size.width * .95,
                    verticalPadding: 20.0,
                    label: isPassenger ?  "Find Ride" : "Create a Trip",
                    textSize: 16,
                    color: _theme.primaryColor,
                    borderColor: _theme.primaryColor,
                    borderRadius: 20.0,
                    onPressed: () {

                         if(pickUpPickedPlace == null) {
                           showToast("Pick up location is required.");
                           return;
                         }

                         if(dropOffPickedPlace == null) {
                           showToast("Drop off location is required.");
                           return;
                         }

                        if(isPassenger) {
                          Navigator.pushNamed(context, availableRidesRoute, arguments: {
                            "pickUpLocation": pickUpPickedPlace,
                            "dropOffLocation": dropOffPickedPlace
                          });
                        }else {
                          Navigator.pushNamed(context, createTripDriverRoute);
                        }
                    }),
              ),
            )
          ],
        ),
      )),
    );
  }

  _locationPickView() {
    return Card(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15)
      ),
      child: Container(
        padding: const EdgeInsets.all(20),
        child: Container(
          margin: const EdgeInsets.only(left: 12),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border:
                        Border.all(width: 5, color: Colors.black)),
                  ),
                  const SizedBox(width: 20,),
                  InkWell(child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children:  [
                      const Text("Choose Pick up", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w700)),
                      Visibility(
                        visible: pickUp != null,
                        child: Container(
                          width: 185,
                          margin: const EdgeInsets.only(top: 3),
                          child: Row(
                            children: [
                              Flexible(
                                child: Text(pickUp != null ? pickUp! : '', style: TextStyle()),
                              )
                            ],
                          ),
                        ),)
                    ],
                  ), onTap: () {
                        Navigator.pushNamed(context, locationSearchScreen).then((value) {
                          if(value != null) {
                            setState(() {
                              pickUpPickedPlace = value as PickedPlace;
                              pickUp = pickUpPickedPlace?.description;
                            });
                          }
                        });
                  },)
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 7),
                    child:  Dash(
                        direction: Axis.vertical,
                        length: 35,
                        dashLength: 1,
                        dashGap: 0,
                        dashColor: Colors.black),
                  ),
                  const SizedBox(width: 15,),
                  const Expanded(
                    child: Divider(height: 3, color: Colors.grey,),
                  )
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(width: 5, color: Colors.black)),
                    child: Container(
                      height: 20,
                    ),
                  ),
                  const SizedBox(width: 20,),

                  InkWell(child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children:  [
                      const Text("Choose Drop off", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w700)),
                      Visibility(
                        visible: dropOff != null,
                        child: Container(
                          width: 185,
                          margin: const EdgeInsets.only(top: 3),
                          child: Row(
                            children: [
                              Flexible(
                                child: Text(dropOff != null ? dropOff! : '', style: TextStyle()),
                              )
                            ],
                          ),
                        ),)
                    ],
                  ), onTap: () {
                    Navigator.pushNamed(context, locationSearchScreen).then((value) {
                      if(value != null) {
                        setState(() {
                          dropOffPickedPlace = value as PickedPlace;
                          dropOff = dropOffPickedPlace?.description;
                        });
                      }
                    });
                  },)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

}