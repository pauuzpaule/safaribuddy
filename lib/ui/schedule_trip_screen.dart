import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/widgets/app_drawer.dart';
import 'package:safaribuddy/widgets/dash.dart';
import 'package:safaribuddy/widgets/form_widgets.dart';

class ScheduleTripScreen extends StatefulWidget {
  const ScheduleTripScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<ScheduleTripScreen> {


  String setDate = "", setTime = "", tripType = kPassengerTrip, start = "Choose start location", destination = "Choose destination";
  bool isPassenger = true;




  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      SharedPrefs().getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: initView(),
          drawer: AppDrawer(),
        ));
  }

  initView() {
    return Container(
      child: Stack(
        fit: StackFit.expand,
        children: [
          GoogleMap(
            myLocationButtonEnabled: false,
            myLocationEnabled: true,
            zoomControlsEnabled: false,
            initialCameraPosition: const CameraPosition(
              target: LatLng(0.3247348, 32.5514714),
              zoom: 16.0,
            ),
            onMapCreated: (GoogleMapController controller) async {


            },
          ),
          Positioned(child: whereToView(), bottom: 0,),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 5),
              child: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    icon: const Icon(
                      Icons.menu,
                      size: 35.0,
                    ),
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    },
                  );
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () {
                Navigator.pushNamed(context, profileRoute);
              },
              child: Container(
                padding: const EdgeInsets.only(right: 20, top: 10),
                child:  const CircleAvatar(
                  radius: 20.0,
                  backgroundImage: AssetImage("assets/images/person.png"),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  whereToView() {
    final ThemeData _theme = Theme.of(context);
    return Card(
      elevation: 20,
      color: Colors.grey[100],
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(40.0),
            topLeft: Radius.circular(40.0),),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(top: 15, left: 10, right: 10),
        margin: const EdgeInsets.only(bottom: 10),
        child: Column(
          children: [
            bottomSheetDash(),
            const SizedBox(height: 10,),
            Row(
              children: [
                Expanded(
                  flex: 1,
                    child: InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, tripLocationRoute);
                      },
                  child: Card(
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)
                    ),
                    child: Container(
                      height: 160,
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          iconWithBg(const Icon(Icons.my_location, size: 32, color: Colors.white,), color: _theme.primaryColor, padding: 15.0),
                          const SizedBox(height: 10,),
                          Text(isPassenger ? 'Find A Buddy' : 'Create Trip', style: const TextStyle(fontSize: 14),),
                          const SizedBox(height: 3,),
                          const Text('View Available Trips', style: TextStyle(fontSize: 12, color: Colors.grey),),
                        ],
                      ),
                    ),
                  ),
                )),
                Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                          Navigator.pushNamed(context, packagesListRoute);
                      },
                      child: Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)
                        ),
                        child: Container(
                          height: 160,
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            children: [
                              iconWithBg(const Icon(Icons.delivery_dining, size: 32, color: Colors.white,), color: _theme.primaryColor, padding: 15.0),
                              const SizedBox(height: 10,),
                              const Text("Packages(Cargo)", textAlign: TextAlign.center,style: TextStyle(fontSize: 14),),
                              const SizedBox(height: 3,),
                              Text(isPassenger ? 'Send small packages and Cargo' : 'View Requests', textAlign: TextAlign.center, style: const TextStyle(fontSize: 12, color: Colors.grey),),
                            ],
                          ),
                        ),
                      ),
                    )),
              ],
            ),
            Row(
              children: [
                Expanded(
                  flex: 1,
                    child: isPassenger ? InkWell(
                      onTap: () {
                          Navigator.pushNamed(context, boatCruiseListRoute);
                      },
                  child: Card(
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)
                    ),
                    child: Container(
                      height: 160,
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          iconWithBg(const Icon(Icons.houseboat, size: 32, color: Colors.white,), color: _theme.primaryColor, padding: 15.0),
                          const SizedBox(height: 10,),
                          const Text("Boat Cruise", style: TextStyle(fontSize: 14),),
                          const SizedBox(height: 3,),
                          const Text('Enjoy your ride today.', style: TextStyle(fontSize: 12, color: Colors.grey),),
                        ],
                      ),
                    ),
                  ),
                ) : InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, driverRides);
                      },
                      child: Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)
                        ),
                        child: Container(
                          height: 160,
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            children: [
                              iconWithBg(const Icon(Icons.bookmark_border, size: 32, color: Colors.white,), color: _theme.primaryColor, padding: 15.0),
                              const SizedBox(height: 10,),
                              const Text("Bookings", style: TextStyle(fontSize: 14),),
                              const SizedBox(height: 3,),
                              const Text('View Available Bookings.', style: TextStyle(fontSize: 12, color: Colors.grey),),
                            ],
                          ),
                        ),
                      ),
                    )),
                Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                          Navigator.pushNamed(context, carHireListRoute);
                      },
                      child: Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)
                        ),
                        child: Container(
                          height: 160,
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            children: [
                              iconWithBg(const Icon(Icons.car_rental, size: 32, color: Colors.white,), color: _theme.primaryColor, padding: 15.0),
                              const SizedBox(height: 10,),
                              const Text("Post Car for hire", style: TextStyle(fontSize: 14),),
                              const SizedBox(height: 3,),
                              const Text('Self drive or with a driver.', textAlign: TextAlign.center, style: TextStyle(fontSize: 12, color: Colors.grey),),
                            ],
                          ),
                        ),
                      ),
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }


  scheduleTrip() {
    setState(() {
      setDate = "";
      setTime = "";
    });
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, _state) {
          return SingleChildScrollView(
            child: Container(
              padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom, left: 20, right: 20, top: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 3,
                        width: 35,
                        decoration: const BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(40.0),
                              bottomRight: Radius.circular(40.0),
                              topLeft: Radius.circular(40.0),
                              bottomLeft: Radius.circular(40.0)),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const SizedBox(height: 20,),

                  Text("Select Car:", style: TextStyle(fontWeight: FontWeight.bold),),
                  Container(
                    height: 150,
                    child: ListView.separated(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: 15,
                      separatorBuilder: (BuildContext context, int index) => SizedBox(width: 10,),
                      itemBuilder: (BuildContext context, int index) => Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)
                        ),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Image.asset(
                                  "assets/images/car_small.png",
                                  height: 100.0,
                                  width: 100.0,
                                ),
                              ),
                              Text("Model T UAB 700F", style: TextStyle(fontSize: 12),)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),

                  Container(height: 20,),

                  Text("Trip Type:", style: TextStyle(fontWeight: FontWeight.bold),),
                  Divider(),
                  Row(
                    children: [
                      Radio(
                        value: kPassengerTrip,
                        groupValue: tripType,
                        onChanged: (value) {
                          modalSetState(_state, () {
                            tripType = value as String;
                          });
                        },
                        activeColor: Colors.green,
                      ),
                      const Text("Passenger"),
                    ],
                  ),
                  Row(
                    children: [
                      Radio(
                        value: kCargoTrip,
                        groupValue: tripType,
                        onChanged: (value) {
                          modalSetState(_state, () {
                            tripType = value as String;
                          });
                        },
                        activeColor: Colors.green,
                      ),
                      const Text("Cargo"),
                    ],
                  ),

                  Container(height: 20,),
                  Text("Location:", style: TextStyle(fontWeight: FontWeight.bold),),
                  Divider(),
                  _tripTimeLine(_state),
                  Divider(),
                  textFieldInput(hintText: "Price per seat", keyboardType: const TextInputType.numberWithOptions()),
                  textFieldInput(hintText: "Number of seats", keyboardType: const TextInputType.numberWithOptions()),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton.icon(onPressed: () {
                        datePicker(context, (date) {
                          modalSetState(_state, () {
                            setDate = date;
                          });
                        });
                      }, icon: const Icon(Icons.calendar_today_outlined), label:  Text(setDate.isNotEmpty ? setDate : "Set date")),
                      TextButton.icon(onPressed: () {
                        timePicker(context, (time) {
                          modalSetState(_state, () {
                            setTime = time;
                          });
                        });
                      }, icon: const Icon(Icons.access_time_outlined), label:  Text (setTime.isNotEmpty ? setTime :"Set time")),
                    ],
                  ),
                  Container(height: 20,),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      formButton(label: "Post Trip", onPressed: () {
                        Navigator.of(context).pop();
                      }),
                    ],
                  ),
                  const SizedBox(height: 20,)

                ],
              ),
            ),
          );
        });
      },
    );
  }

  _tripTimeLine(_state) {
    return Container(
      margin: EdgeInsets.only(left: 12),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    color: Colors.green,
                    shape: BoxShape.circle,
                    border:
                    Border.all(width: 1.5, color: Colors.green)),
              ),
              SizedBox(width: 20,),
              InkWell(child: Text(start, style: TextStyle(color: Colors.blueAccent),), onTap: () {
                Navigator.pushNamed(context, locationSearchScreen).then((value) {
                  modalSetState(_state, () {
                    start = "Mulago Rd, Kampala";
                  });
                });
              },)
            ],
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 10),
                child:  Dash(
                    direction: Axis.vertical,
                    length: 30,
                    dashLength: 1,
                    dashColor: Colors.black),
              ),
              Divider(height: 2,)
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    color: Colors.red,
                    shape: BoxShape.rectangle,
                    border: Border.all(width: 2, color: Colors.red)),
                child: Container(
                  height: 20,
                ),
              ),
              SizedBox(width: 20,),
              InkWell(child: Text(destination, style: TextStyle(color: Colors.blueAccent),), onTap: () {
                Navigator.pushNamed(context, locationSearchScreen).then((value) {
                  modalSetState(_state, () {
                    destination = "Bukoto cresent, Kampala";
                  });
                });
              },)
            ],
          )
        ],
      ),
    );
  }

}