import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/util/custom_widgets.dart';

class SearchRiderScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<SearchRiderScreen> {

  final Completer<GoogleMapController> _controller = Completer();
  bool isSearching = true;

  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 10), () {
      setState(() {
        isSearching = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        title: const Text(
          "Ride details",
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        child: initView(),
      ),
    );
  }

  initView() {
    final ThemeData _theme = Theme.of(context);
    return Container(
      child: Column(
        children: [
          Expanded(
            child: GoogleMap(
              myLocationButtonEnabled: true,
              myLocationEnabled: true,
              initialCameraPosition: const CameraPosition(
                target: LatLng(0.3247348, 32.5514714),
                zoom: 11.0,
              ),
              onMapCreated: (GoogleMapController controller) async {
                _controller.complete(controller);
              },
            ),
          ),
          searchView()

        ],
      ),
    );
  }

  searchView() {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Wrap(
        children: [
          Visibility(
            visible: !isSearching,
              child: Expanded(child: Column(
            children:  [
              ListTile(
                leading: const CircleAvatar(
                  radius: 30.0,
                  backgroundImage: AssetImage("assets/images/person.png"),
                ),
                title: const Text("Pauuz Sayrunja"),
                subtitle: const Text("5 mins"),
                trailing: Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: const [
                    Icon(Icons.star), SizedBox(width: 2,), Text("4.3")
                  ],
                ),
              ),
              const Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  formButton(label: "Call", onPressed: () {}, borderRadius: 15),
                  const SizedBox(width: 10,),
                  formButton(label: "Cancel", onPressed: () {}, borderRadius: 15, color: Colors.red, borderColor: Colors.red),
                ],
              )
            ],
          ))),
          Visibility(
            visible: isSearching,
              child: Expanded(child: Column(children:  [
                const  SizedBox(
                  child: CircularProgressIndicator(strokeWidth: 2,),
                height: 50.0,
                width: 50.0,
              ),
                const Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    formButton(label: "Cancel", onPressed: () {}, borderRadius: 15, color: Colors.red, borderColor: Colors.red),
                  ],
                )
              ],)))
        ],
      ),
    );
  }
}
