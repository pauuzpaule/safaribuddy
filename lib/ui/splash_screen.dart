
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:safaribuddy/data/auth_obj.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<SplashScreen> {


  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      Timer(const Duration(seconds: 5), () async {

        AuthObj? authObj = await SharedPrefs().getAuthObj();

        if(authObj != null) {
          Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
        }else {
          Navigator.pushNamedAndRemoveUntil(context, loginRoute, (route) => false);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Center(
          child: Image.asset("assets/icons/logo.png"),
        ),
      ),
    );
  }

}