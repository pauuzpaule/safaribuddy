import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bubble/bubble.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/data/trip.dart';
import 'package:safaribuddy/util/color_utils.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/viewModel/trip_view_model.dart';
import 'package:safaribuddy/widgets/dash.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:url_launcher/url_launcher.dart';

class TripDetailsScreen extends StatefulWidget {
  const TripDetailsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<TripDetailsScreen> {
  bool isPassenger = true;
  int _current = 0;
  final CarouselController _controller = CarouselController();
  final TextEditingController _seatsController = TextEditingController();

  int noOfSeats = 1;
  int pricePerSeat = 30000;
  int totalPrice = 30000;
  bool availableSeatLimitReached = false;

  Trip? trip;

  bool hasCar = false;
  bool ownTrip = false;
  late final Set<Marker> _markers = {};

  bool isLoading = false;

  static GlobalKey<NavigatorState> navigatorKey =
  GlobalKey<NavigatorState>();

  final List<String> imgList = [
    'https://vimg.cheki.co.ug/xl/1_inventory1860053_1549052620.jpg',
    'https://vimg.cheki.co.ug/xl/1_inventory1860053_1549052625.jpg',
    'https://vimg.cheki.co.ug/xl/1_inventory1860053_1549052588.jpg',
    'https://vimg.cheki.co.ug/xl/1_inventory1860053_1549052602.jpg'
  ];

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      _seatsController.text = noOfSeats.toString();
      SharedPrefs().getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });

      try {
        var intentData = ModalRoute.of(context)!.settings.arguments as Map;
        if (intentData.containsKey("trip")) {
          setState(() {
            trip = intentData["trip"] as Trip;
            pricePerSeat = trip!.pricePerSeat!;
            totalPrice = trip!.pricePerSeat!;
          });
        }

        if (intentData.containsKey("ownTrip")) {
          setState(() {
            ownTrip = intentData["ownTrip"] as bool;
          });
        }
      } catch (e, trace) {
        print(e);
        print(trace);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return hasCar ? _viewWithCar() : _viewWithoutCar();
  }

  _viewWithoutCar() {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
     key: navigatorKey,
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "Trip",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: initView(),
    );
  }

  _viewWithCar() {
    final ThemeData _theme = Theme.of(context);
    final screenHeight = MediaQuery.of(context).size.height;
    return SafeArea(
        child: Scaffold(
            body: NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          SliverAppBar(
            expandedHeight: screenHeight * .4,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: indicators(),
                background: Stack(
                  children: [
                    CarouselSlider(
                      items: imageSliders(),
                      carouselController: _controller,
                      options: CarouselOptions(
                          height: screenHeight * 4,
                          autoPlay: true,
                          //enlargeCenterPage: true,
                          aspectRatio: 1.0,
                          viewportFraction: 1,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                            });
                          }),
                    )
                  ],
                )),
          ),
        ];
      },
      body: initView(),
    )));
  }

  initView() {
    final ThemeData _theme = Theme.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            width: double.infinity,
            height: 300,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
            child: Stack(
              fit: StackFit.expand,
              children: [
                _map(),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            "(${trip?.type} Trip)",
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Text(
                "${trip?.formatPickupDate()}",
                style:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              const Spacer(),
              Text(
                  "${trip?.pricePerSeat != null ? formatMoney(trip?.pricePerSeat) : ''}",
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.bold))
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          _tripTimeLine(),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Text(
                "${trip?.availableSeats()} Seats available",
                textAlign: TextAlign.left,
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Visibility(
            visible: trip?.driver != null,
            child: _driverDetails(),
          ),
          const Spacer(),
          Visibility(
              visible: isPassenger,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                 Visibility(
                   visible: trip?.driver != null,
                   child:  Container(
                       padding: const EdgeInsets.all(10),
                       margin: const EdgeInsets.only(right: 10),
                       decoration: ShapeDecoration(
                         color: Colors.grey[300],
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(10)),
                       ),
                       child: InkWell(
                         child: const Icon(
                           Icons.call,
                         ),
                         onTap: () async {
                           Driver driver = trip!.driver!;
                           User driverProfile = driver.user;
                           var re = RegExp(r'\d{3}');
                           var phone = driverProfile.phoneNumber.replaceFirst(re, '0');
                           var url = "tel:$phone";
                           if (await canLaunch(url)) {
                           await launch(url);
                           } else {
                           throw 'Could not launch $url';
                           }
                         },
                       )),
                 ),
                  Visibility(
                    visible: trip?.driver != null,
                    child: Container(
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.only(right: 10),
                        decoration: ShapeDecoration(
                          color: Colors.grey[300],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        child: InkWell(
                          child: const Icon(
                            Icons.chat,
                          ),
                          onTap: () {},
                        )),
                  ),
                  Visibility(
                    visible: ownTrip && trip!.tripBids!.isNotEmpty && trip?.driver == null,
                    child: formButton(
                        label: "View Bids",
                        borderRadius: 10,
                        onPressed: () {
                          _viewTripBids(trip!);
                        },
                        borderColor: _theme.primaryColor,
                        color: _theme.primaryColor,
                        verticalPadding: 15,
                        horizontalPadding: 50),
                  ),
                  Visibility(
                    visible: !ownTrip,
                    child: isLoading ? loadingView() :formButton(
                        label: "Book now",
                        borderRadius: 10,
                        onPressed: () {
                          _bookingDialogDetails();
                        },
                        borderColor: _theme.primaryColor,
                        color: _theme.primaryColor,
                        verticalPadding: 15,
                        horizontalPadding: 50),
                  )
                ],
              )),
          Visibility(
              visible: !isPassenger,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  formButton(
                      label: "View Request",
                      borderRadius: 10,
                      onPressed: () {
                        Navigator.pushNamed(context, tripRequestRoute);
                      },
                      textColor: Colors.black,
                      borderColor: Colors.white,
                      color: Colors.white,
                      verticalPadding: 15,
                      horizontalPadding: 25),
                  const SizedBox(
                    width: 10,
                  ),
                  formButton(
                      label: "Start Trip",
                      borderRadius: 10,
                      onPressed: () {},
                      borderColor: _theme.primaryColor,
                      color: _theme.primaryColor,
                      verticalPadding: 15,
                      horizontalPadding: 50),
                ],
              ))
        ],
      ),
    );
  }

  Widget indicators() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: imgList.asMap().entries.map((entry) {
        return GestureDetector(
          onTap: () => _controller.animateToPage(entry.key),
          child: Container(
            width: 6.0,
            height: 6.0,
            margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: (Theme.of(context).brightness == Brightness.dark
                        ? Colors.grey
                        : Colors.white)
                    .withOpacity(_current == entry.key ? 0.9 : 0.4)),
          ),
        );
      }).toList(),
    );
  }

  List<Widget> imageSliders() {
    final screenHeight = MediaQuery.of(context).size.height;
    return imgList
        .map((item) => Image.network(item, fit: BoxFit.fill))
        .toList();
  }

  _tripTimeLine() {
    return Column(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 10,
              width: 10,
              decoration: BoxDecoration(
                  color: Colors.black,
                  shape: BoxShape.circle,
                  border: Border.all(width: 1.5, color: Colors.black)),
            ),
            const SizedBox(
              width: 10,
            ),
            Flexible(child: Text("${trip?.pickupAddress}"))
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.only(left: 4),
              child: Dash(
                  direction: Axis.vertical,
                  length: 18,
                  dashLength: 1,
                  dashGap: 0,
                  dashColor: Colors.black),
            )
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 10,
              width: 10,
              decoration: BoxDecoration(
                  color: Colors.black,
                  shape: BoxShape.rectangle,
                  border: Border.all(width: 2, color: Colors.black)),
              child: Container(
                height: 20,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Flexible(child: Text("${trip?.dropOffAddress}"))
          ],
        )
      ],
    );
  }

  _map() {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(20),
        topRight: Radius.circular(20),
        bottomRight: Radius.circular(20),
        bottomLeft: Radius.circular(20),
      ),
      child: Align(
        alignment: Alignment.bottomRight,
        heightFactor: 0.3,
        widthFactor: 2.5,
        child: GoogleMap(
          myLocationButtonEnabled: true,
          myLocationEnabled: true,
          zoomControlsEnabled: false,
          initialCameraPosition: const CameraPosition(
            target: LatLng(0.3247348, 32.5514714),
            zoom: 11.0,
          ),
          markers: _markers,
          onMapCreated: (GoogleMapController controller) async {
            setState(() {
              if (trip != null) {
                _markers.add(
                  Marker(
                    markerId: const MarkerId('pick-up-marker'),
                    position: LatLng(
                        trip!.pickuplat.toDouble(), trip!.pickuplng.toDouble()),
                  ),
                );
                _markers.add(
                  Marker(
                    markerId: const MarkerId('drop-off-marker'),
                    position: LatLng(trip!.dropofflat.toDouble(),
                        trip!.dropofflng.toDouble()),
                  ),
                );
              }
            });
          },
        ),
      ),
    );
  }

  _driverDetails() {
    final ThemeData _theme = Theme.of(context);
    return Row(
      children: [
        const CircleAvatar(
          radius: 15.0,
          backgroundImage: AssetImage("assets/images/person.png"),
        ),
        const SizedBox(
          width: 10,
        ),
        Text("${trip?.driver?.user.firstname} ${trip?.driver?.user.lastname}"),
        const Spacer(),
        RatingBarIndicator(
          rating: 4,
          itemCount: 5,
          itemSize: 20.0,
          physics: const BouncingScrollPhysics(),
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: _theme.primaryColor,
          ),
        ),
      ],
    );
  }

  _bookingDialogDetails() {
    final ThemeData _theme = Theme.of(context);
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return StatefulBuilder(
            builder: (context, state) {
              return Container(
                padding: const EdgeInsets.all(20),
                child: Wrap(
                  children: [
                    const Text(
                      'Confirm Booking',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      height: 20,
                    ),
                    Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Container(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children:  [
                                      Text('${trip!.formatPickupDate(outputFormat: 'HH:mm a')}'),
                                      Text('${trip!.formatPickupDate(outputFormat: 'E d, MMM')}'),
                                    ],
                                  ),
                                ),
                                const Spacer(),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text('${trip!.availableSeats()} Seats'),
                                      Text.rich(
                                        TextSpan(
                                          children: [
                                            TextSpan(
                                              text:
                                                  'UGX ${formatMoney(pricePerSeat, currency: '')}',
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            const TextSpan(
                                                text: ' per seat',
                                                style: TextStyle(fontSize: 12)),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 10,
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "Number of Seats",
                            style: TextStyle(),
                          ),
                          Spacer(),
                          Container(
                              padding: const EdgeInsets.all(5),
                              decoration: ShapeDecoration(
                                color: Colors.grey[200],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                              child: InkWell(
                                child: Icon(
                                  Icons.add,
                                ),
                                onTap: () {
                                  _increaseSeats(state);
                                },
                              )),
                          Container(
                            width: 35,
                            height: 35,
                            margin: const EdgeInsets.symmetric(horizontal: 10),
                            child: TextField(
                              controller: _seatsController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  filled: true,
                                  hintStyle:
                                      const TextStyle(color: Colors.grey),
                                  fillColor: Colors.white70),
                            ),
                          ),
                          Container(
                              padding: const EdgeInsets.all(5),
                              decoration: ShapeDecoration(
                                color: Colors.grey[200],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                              child: InkWell(
                                child: const Icon(
                                  Icons.remove,
                                ),
                                onTap: () {
                                  _decreaseSeats(state);
                                },
                              )),
                        ],
                      ),
                    ),
                    Container(
                      height: 10,
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        children: [
                          const Text('Price'),
                          const SizedBox(
                            width: 10,
                          ),
                          Text('UGX ${formatMoney(totalPrice, currency: '')}'),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    Container(
                      child: isLoading ? loadingView() : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FloatingActionButton(
                              backgroundColor: _theme.primaryColor,
                              child: const Icon(
                                Icons.check,
                                color: Colors.white,
                              ),
                              onPressed: () {

                                Navigator.of(context).pop();

                                var request = {
                                  "tripId": trip!.id.toString(),
                                  "numberOfSeats": _seatsController.text
                                };

                                TripViewModel tripViewModel = Provider.of<TripViewModel>(context, listen: false);
                                modalSetState(state, () {
                                  isLoading = true;
                                });

                                tripViewModel.bookTrip(request, context: context).whenComplete(() {
                                  modalSetState(state, () {
                                    isLoading = false;
                                  });

                                  if (tripViewModel
                                      .tripBookedSuccessfully!) {

                                    aweSomeDialog(
                                        context: navigatorKey.currentContext,
                                        dialogType: DialogType.SUCCES,
                                        title: "",
                                        desc:
                                        "Trip has been booked.\n Driver will contact you shortly",
                                        btnOkPress: () {
                                          Navigator.of(context).pop();
                                        });


                                  } else {
                                    String error =
                                    tripViewModel.bookTripError!;


                                    if (error != kInternetError) {
                                      showSnackBar(context, error,
                                          seconds: 5,
                                          action: SnackBarAction(
                                              label: "Ok",
                                              onPressed: () async {}));
                                    }
                                  }

                                });



                                print("Request $request");


                              })
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        });
  }

  _viewTripBids(Trip trip) {
    final ThemeData _theme = Theme.of(context);
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return StatefulBuilder(
            builder: (ctx, state) {
              return Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Visibility(
                      visible: isLoading,
                      child: loadingView(),
                    ),
                    Expanded(child: ListView.builder(
                        itemBuilder: (context, pos) {
                          TripBid bid = trip.tripBids![pos];
                          return Card(
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              child: ListTile(
                                leading: bid.driver?.user != null &&
                                    bid.driver?.user.profilePic != null
                                    ? CircleAvatar(
                                  radius: 35.0,
                                  backgroundImage: NetworkImage(
                                      bid.driver?.user.profilePic),
                                  backgroundColor: Colors.transparent,
                                )
                                    : const CircleAvatar(
                                  radius: 35.0,
                                  backgroundImage:
                                  AssetImage('assets/images/person.png'),
                                ),
                                title: Text("${bid.driver?.user.fullName()}"),
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text("${bid.driver?.user.phoneNumber}"),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    tagLabel(
                                        Text(
                                          bid.status,
                                          style: const TextStyle(
                                              color: Colors.white, fontSize: 12),
                                        ),
                                        color: _statusColor(bid.status))
                                  ],
                                ),
                                trailing: InkWell(
                                  onTap: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: const Text(
                                              'Review Trip Bid'),
                                          content: SingleChildScrollView(
                                            child: ListBody(
                                              children:  <Widget>[
                                                Text(
                                                    'Accept/Reject ${bid.driver?.user.fullName()} as this trip driver.'),
                                              ],
                                            ),
                                          ),
                                          actions: <Widget>[
                                            TextButton(
                                              child: const Text('CANCEL'),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                            TextButton(
                                              child: const Text('REJECT', style: TextStyle(color: Colors.redAccent)),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                                _reviewBidCall(bid.id, "rejected", state, ctx);
                                              },
                                            ),
                                            TextButton(
                                              child: const Text('ACCEPT', style: TextStyle(color: Colors.green)),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                                _reviewBidCall(bid.id, "accepted", state, ctx);
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                  child: const Icon(Icons.arrow_forward_ios),
                                ),
                              ),
                            ),
                          );
                        },
                        itemCount: trip.tripBids!.length))
                  ],
                ),
              );
            },
          );
        });
  }

  _reviewBidCall(tripBidId, status, sheetStatus, sheetContext) {
    var request = {
      "tripBidId": tripBidId.toString(),
      "status": status
    };
    TripViewModel tripViewModel = Provider.of<TripViewModel>(context, listen: false);
    modalSetState(sheetStatus, () {
      isLoading = true;
    });

    tripViewModel.reviewTripBid(request, context: context).whenComplete(() {
      modalSetState(sheetStatus, () {
        isLoading = false;
      });

      if (tripViewModel
          .tripBidReviewedSuccessfully!) {

        modalSetState(sheetStatus, () {
          trip = tripViewModel.reviewedTrip!;
        });

        TripBid tripBid = tripViewModel.reviewedTripBid!;
        if(tripBid.status == "accepted") {
          aweSomeDialog(
              context: context,
              dialogType: DialogType.SUCCES,
              title: "",
              desc:
              "Driver has been notified.",
              btnOkPress: () {
                Navigator.of(sheetContext).pop();
              });
        }else {
          aweSomeDialog(
              context: context,
              dialogType: DialogType.ERROR,
              title: "",
              desc:
              "Trip bid rejected.",
              btnOkPress: () {
                Navigator.of(sheetContext).pop();
              });
        }


      } else {
        String error =
        tripViewModel.reviewBidError!;
        if (error != kInternetError) {
          showSnackBar(context, error,
              seconds: 5,
              action: SnackBarAction(
                  label: "Ok",
                  onPressed: () async {}));
        }
      }
    });

  }

  _statusColor(status) {
    if (status == "accepted") {
      return Colors.green;
    } else if (status == "rejected") {
      return Colors.redAccent;
    } else {
      return Colors.deepOrangeAccent[200];
    }
  }

  _increaseSeats(state) {
    modalSetState(state, () {
      if (noOfSeats < trip!.availableSeats()) {
        noOfSeats++;
        totalPrice = pricePerSeat * noOfSeats;
        _seatsController.text = noOfSeats.toString();
      } else {
        showToast("Limit reached");
        availableSeatLimitReached = true;
      }
    });
  }

  _decreaseSeats(state) {
    modalSetState(state, () {
      if (noOfSeats > 1) {
        noOfSeats--;
        totalPrice = pricePerSeat * noOfSeats;
        _seatsController.text = noOfSeats.toString();
        availableSeatLimitReached = false;
      }
    });
  }
}
