import 'package:flutter/material.dart';
import 'package:safaribuddy/widgets/ride_cards.dart';

class TripRequestsScreen extends StatefulWidget {
  const TripRequestsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<TripRequestsScreen> {



  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    int bookings = 2;

    try{
      var intentData = ModalRoute.of(context)!.settings.arguments as int;
      bookings = intentData;
    }catch(e) {}

    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: const Text(
          "Trip Requests",
          style: TextStyle(color: Colors.black87),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(15.0),
        child:   ListView.separated(
          shrinkWrap: true,
          itemCount: bookings,
          separatorBuilder: (BuildContext context, int index) =>
              const SizedBox(
                height: 10,
              ),
          itemBuilder: (BuildContext context, int index) => Card(
            elevation: 0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5)),
            child: Container(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.asset(
                      "assets/images/person.png",
                      height: 40.0,
                      width: 40.0,
                    ),
                  ),
                  SizedBox(width: 20,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Pauuz Sayrunjah",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "2 Seats booked",
                      )
                    ],
                  ),
                  const Spacer(),
                  Container(
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        shape: BoxShape.circle,
                      ),
                      child: InkWell(
                        child: const Icon(
                          Icons.call,
                        ),
                        onTap: () {},
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

}