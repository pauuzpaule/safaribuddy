import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/data/car.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/viewModel/car_view_model.dart';

class UserCarsScreen extends StatefulWidget {
  const UserCarsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<UserCarsScreen> {

  List<Car> carList = [];

  @override
  void initState() {
    super.initState();

    print("Helodsadsfsaf dfasfds");
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {

      var carViewModel = Provider.of<CarViewModel>(context, listen: false);
      carViewModel.getUserCars(context: context).whenComplete(() {
        setState(() {
          carList = carViewModel.carList;
        });
      });

    });

  }

  @override
  Widget build(BuildContext context) {
    CarViewModel carViewModel = Provider.of<CarViewModel>(context, listen: false);
    return Scaffold(
      appBar: appBar(context, "Cars"),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: carViewModel.isLoading ? loadingView() : carListView(carViewModel),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black87,
        child: Icon(Icons.add, color: Colors.white),
        onPressed: () {
        Navigator.pushNamed(context, createCarRoute);
      },

      ),
    );
  }


  carListView(CarViewModel carViewModel) {
    return carList.isEmpty ? Container(
      child: Center(child: Text("No Cars registered."),),
    ) : Container(
      child: ListView.separated(itemBuilder: (context, pos) {

        Car car =  carList[pos];
        return Card(
          elevation: 0,
          child: Container(
            padding: const EdgeInsets.all(10),
            child: ListTile(
              leading: FancyShimmerImage(
                imageUrl: car.images != null && car.images!.isNotEmpty ? car.images!.first.image.toString() : '',
                height: 110,
                width: 120,
                boxFit: BoxFit.fill,
              ),
              title: Text(car.name),
              subtitle: Text(car.model),
            ),
          ),
        );
      }, separatorBuilder:  (context, pos) {
          return const SizedBox(height: 5,);
      }, itemCount: carList.length),
    );
  }

}