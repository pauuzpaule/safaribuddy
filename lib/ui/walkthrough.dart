import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safaribuddy/providers/walk_through_provider.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/widgets/walkthrough_stepper.dart';
import 'package:safaribuddy/widgets/walkthrough_template.dart';

class WalkThroughScreen extends StatelessWidget {
  final PageController _pageViewController = PageController(initialPage: 0);
  @override
  Widget build(BuildContext context) {
    final WalkThroughProvider _walkthroughProvider =
        Provider.of<WalkThroughProvider>(context, listen: false);
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: PageView(
                  controller: _pageViewController,
                  onPageChanged: (int index) {
                    _walkthroughProvider.onPageChange(index);
                  },
                  children: <Widget>[
                    WalkThroughTemplate(
                      title: "Pay with your mobile",
                      subtitle:
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quis dui justo. Sed eget efficitur magna, sollicitudin iaculis ipsum. Proin vitae metus ut velit euismod mattis ut non mi. Integer congue",
                      image: Image.asset("assets/images/walkthrough1.png"),
                    ),
                    WalkThroughTemplate(
                      title: "Get bonuses on each ride",
                      subtitle:
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quis dui justo. Sed eget efficitur magna, sollicitudin iaculis ipsum. Proin vitae metus ut velit euismod mattis ut non mi. Integer congue",
                      image: Image.asset("assets/images/walkthrough2.png"),
                    ),
                    WalkThroughTemplate(
                      title: "Invite friends and get paid.",
                      subtitle:
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quis dui justo. Sed eget efficitur magna, sollicitudin iaculis ipsum. Proin vitae metus ut velit euismod mattis ut non mi. Integer congue.",
                      image: Image.asset("assets/images/walkthrough3.png"),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(24.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child:
                          WalkthroughStepper(controller: _pageViewController),
                    ),
                    ClipOval(
                      child: Container(
                        color: Theme.of(context).primaryColor,
                        child: IconButton(
                          icon: const Icon(
                            Icons.trending_flat,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            if (_pageViewController.page! >= 2) {
                              Navigator.of(context).pushReplacementNamed(
                                  loginRoute);
                              return;
                            }
                            _pageViewController.nextPage(
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.ease);
                          },
                          padding: const EdgeInsets.all(13.0),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
