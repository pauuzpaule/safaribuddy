import 'package:flutter/material.dart';
import 'hext_color.dart';

appBlack() {
  return HexColor("#17333F");
}

accentColor() {
  return HexColor("#FF5C5C");
}

secondaryColor() {
  return HexColor("#B7CD02");
}
secondaryBlueColor() {
  return HexColor("#04ACEA");
}

primaryColor () {
  return HexColor("#17333F");
}
primaryDarkColor () {
  return HexColor("#2A3664");
}

primaryDarkerColor () {
  return HexColor("#283e8a");
}

primaryLightColor () {
  return HexColor("#00AEEF");
}

primaryLightColor2 () {
  return HexColor("#00AEEF");
}

textColor () {
  return Colors.black87;
}

backgroundColor () {
  return HexColor("#F3F5F9");
}

lightBackGround() {
  return HexColor("#EFF7FA");
}

lightGrayColor() {
  return HexColor("#D5DADF");
}

lightGreyColor() {
  return HexColor("#D7E4FF");
}

lightGreen() {
  return HexColor("#06C4A5");
}

simpleWhiteColor() {
  return HexColor("#E0E0F4");
  //return HexColor("#F6F7FC");
}

darkTextColor() {
  return HexColor("#788390");
}

createMaterialColor(color) {
  return  MaterialColor(
    color,
    <int, Color>{
      50:  Color(color),
      100: Color(color),
      200: Color(color),
      300: Color(color),
      400: Color(color),
      500: Color(color),
      600: Color(color),
      700: Color(color),
      800: Color(color),
      900: Color(color),
    },
  );
}