import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:http_parser/http_parser.dart';
import 'package:safaribuddy/util/shared_prefs.dart';
import 'package:safaribuddy/util/utils.dart';

//final appurl = "http://192.168.43.241:8000";
final appurl = "https://gs-virtual-learning.herokuapp.com";


Future<http.Response> makeCall(url) async {
  final _fullurl = await SharedPrefs().getAppURL() + url;

  print(_fullurl);
  final _sharedPrefs = SharedPrefs();
  var accessToken = await _sharedPrefs.getAccessToken();

  if (accessToken != null) {
    var headers = {
      'Authorization': 'Bearer $accessToken',
    };
    return await http.get(Uri.parse(_fullurl), headers: headers);
  }else {
    return await http.get(Uri.parse(_fullurl));
  }
}


Future<http.Response> makePostCall(url, body) async {
  final _fullurl = await SharedPrefs().getAppURL() + url;

  print(_fullurl);

  final _sharedPrefs = SharedPrefs();
  var accessToken = await _sharedPrefs.getAccessToken();

  print("Request body:: $body");

  if (accessToken != null) {
    var headers = {
      'Authorization': 'Bearer $accessToken',
    };
    var response = await http.post(Uri.parse(_fullurl), body: body, headers: headers);
    print("Response:: ${response.body}");
    return response;
  }else {
    var response = await http.post(Uri.parse(_fullurl), body: body);
    print("Response:: ${response.body}");
    return response;
  }
}

Future<http.Response> makePatchCall(url, body) async {
  final _fullurl = await SharedPrefs().getAppURL() + url;

  print(_fullurl);

  final _sharedPrefs = SharedPrefs();
  var accessToken = await _sharedPrefs.getAccessToken();

  print("token: $accessToken");
  if (accessToken != null) {
    var headers = {
      'Authorization': 'Bearer $accessToken',
    };
    return await http.patch(Uri.parse(_fullurl), body: body, headers: headers);
  }else {
    return await http.patch(Uri.parse(_fullurl), body: body);
  }
}


Future<http.StreamedResponse> makeMultipartCall(url,File imageFile, {required fileParam , method: "POST", body, mimeType: 'image'}) async {
  final _fullurl = await SharedPrefs().getAppURL() + url;

  final _sharedPrefs = SharedPrefs();
  var accessToken = await _sharedPrefs.getAccessToken();

  print("token: $accessToken");

  var stream =
      new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
  var length = await imageFile.length();

  var uri = Uri.parse(_fullurl);

  var request = new http.MultipartRequest(method, uri);

  if(body != null) {
    body.forEach((k, v) {
      request.fields[k] = v;
    });
  }
  var multipartFile = new http.MultipartFile(fileParam, stream, length,
      filename: basename(imageFile.path), contentType: MediaType(mimeType, getFileExtension(imageFile.path)));

  request.files.add(multipartFile);
  if(accessToken != null) {
    print("Access Token: $accessToken}");
    request.headers.addAll({
      'Authorization': 'Bearer $accessToken',
    });
  }


  return await request.send();
}



Future<http.Response> makeSimpleRequest(url, {accessToken}) async {
  if(accessToken != null) {
    var headers = {
      'Authorization': 'Bearer $accessToken',
    };
    return await http.get(Uri.parse(url), headers: headers);
  }else {
    return await http.get(Uri.parse(url));
  }

}

Future downloadFile(String url, String savePath, showDownloadProgress) async {
  try {
    var dio = Dio();
    return await dio.download(url, savePath, onReceiveProgress: showDownloadProgress);
  } catch (e) {
    print(e);
  }
}


