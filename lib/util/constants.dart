const kAppName = "SafariBuddy";
const kDriver = "driver";
const kPassenger = "passenger";

const kPassengerTrip = "passengerTrip";
const kCargoTrip = "cargoTrip";

const kSelfDrive = "selfDrive";
const kDesignatedDriver = "designatedDriver";

const kPackage = "package";
const kCargo = "cargo";

const kProdEnv = "prodEnv";
const kDummyText  = "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged";

const kSuccess = "success";
const kFailed = "failed";

const kProdUrl = "http://192.168.43.241:4000";
const kInternetError = "noInternet";
const kServerError = "Internal Server Error";