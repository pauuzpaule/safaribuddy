import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:safaribuddy/util/color_utils.dart';

appButton({required label, required onPressed, Color? color, Color? textColor}) {
  return MaterialButton(
    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
    color: color ?? primaryColor(),
      minWidth: double.infinity,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10)
    ),
    child: Text(label, style: TextStyle(color: textColor ?? Colors.white),),
      onPressed: onPressed);
}


formButton(
    {required label,
      onPressed,
      verticalPadding= 10.0,
      horizontalPadding= 45.0,
      color,
      borderColor,
      borderRadius= 5.0,
      textSize= 12.0,
      minWidth = 30.0,
      textColor= Colors.white}) {
  return MaterialButton(
    minWidth: minWidth,
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius.toDouble()),
        side: BorderSide(color: borderColor ?? primaryColor())),
    padding: EdgeInsets.symmetric(
        vertical: verticalPadding.toDouble(), horizontal: horizontalPadding.toDouble()),
    child: Text(
      label,
      style: TextStyle(color: textColor, fontSize: textSize.toDouble()),
    ),
    color: color ?? primaryColor(),
    onPressed: onPressed,
  );
}

bottomSheetDash() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        height: 3.5,
        width: 55,
        decoration: const BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(40.0),
              bottomRight: Radius.circular(40.0),
              topLeft: Radius.circular(40.0),
              bottomLeft: Radius.circular(40.0)),
        ),
      ),
    ],
  );
}

Future<void> datePicker (BuildContext context, onSelected) async {
  var today = DateTime.now();

  var lastDate = DateTime(today.year+ 2);

  final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: today,
      firstDate: today,
      lastDate: lastDate);
  if (picked != null) {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    onSelected(formatter.format(picked));
  }
}

Future<void> timePicker(BuildContext context, onSelected) async {
  TimeOfDay selectedTime = TimeOfDay.now();
  final TimeOfDay? picked_s = await showTimePicker(
      context: context,
      initialTime: selectedTime);

  if (picked_s != null && picked_s != selectedTime ) {
    var today = DateTime.now();
    DateTime _dt = DateTime(
      today.year,
      today.month,
      today.day,
      picked_s.hour,
      picked_s.minute,
    );
    final DateFormat formatter = DateFormat('HH:mm');
    onSelected(formatter.format(_dt));
  }
}

tagLabel(Text text, {color, padding = 5.0}) {
  return Container(
    padding:  EdgeInsets.all(padding),
    child: text,
    decoration: BoxDecoration(
        color: color ?? primaryColor(),
        borderRadius: BorderRadius.circular(8)
    ),
  );
}

iconWithBg(Icon icon, {color, padding = 5.0}) {
  return Container(
    padding:  EdgeInsets.all(padding),
    child: icon,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
        color: color ?? primaryColor(),
    ),
  );
}

loadingView({color, strokeWidth = 2}) {
  return Center(
    child: CircularProgressIndicator(
      color: color,
      strokeWidth: strokeWidth.toDouble(),
    ),
  );
}

appBar(context, title) {
  final ThemeData _theme = Theme.of(context);
  return AppBar(
    backgroundColor: _theme.scaffoldBackgroundColor,
    automaticallyImplyLeading: false,
    elevation: 0.0,
    centerTitle: true,
    title:  Text(
      title,
      style: const TextStyle(color: Colors.black87),
    ),
    leading: IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
      },
    ),
  );
}