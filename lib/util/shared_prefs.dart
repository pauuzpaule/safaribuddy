
import 'dart:convert';

import 'package:safaribuddy/data/auth_obj.dart';
import 'package:safaribuddy/data/place_search.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  late SharedPreferences sharedPreferences;
  static const String tagUserType = "userType";
  static const String tagUrl = "tagUrl";
  static const String tagAccessToken = "accessToken";
  static const String tagAuthObj = "authObj";
  static const String tagGooglePlaceSearch = "googlePlaceSearch";

  SharedPrefs()  {
        () async{
      sharedPreferences = await SharedPreferences.getInstance();
    }();
  }

  storeAppURL(String type) async{
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(tagUrl, type);
  }

  getAppURL() async{
    sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(tagUrl) ?? kProdUrl;
  }

  saveUserType(String type) async{
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(tagUserType, type);
  }

  Future<String> getUserType() async{
    sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(tagUserType) ?? kPassenger;
  }

  removeAllData() async {
    SharedPreferences preferences =
    await SharedPreferences.getInstance();
    preferences.clear();
  }

  storeAccessToken(String type) async{
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(tagAccessToken, type);
  }

  getAccessToken() async{
    AuthObj? authObj  = await getAuthObj();
    return authObj?.token;
  }

  storeAuthObj(AuthObj obj) async{
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(tagAuthObj, obj.toJson());
  }

  Future<AuthObj?> getAuthObj() async{
    sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(tagAuthObj) != null ?  AuthObj.fromJson(sharedPreferences.getString(tagAuthObj)!) : null;
  }

  storeCachedPlaces(List<PlaceSearch> list) async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(tagGooglePlaceSearch, PlaceSearch.encode(list));
  }

  Future<List<PlaceSearch>> getCachedPlaces() async {
    sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(tagGooglePlaceSearch) != null ? PlaceSearch.decode(sharedPreferences.getString(tagGooglePlaceSearch)!) : [];
  }
}