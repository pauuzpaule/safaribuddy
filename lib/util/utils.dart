import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';


dateFromUtcToLocal(dateUtc, {format = 'yyyy-MM-ddTHH:mm:ss.000000Z'}) {
  var dateTime = DateFormat(format).parse(dateUtc, true);
  return dateTime.toLocal();
}

dateFromLocalToUtc(dateLocal, {format: 'yyyy-MM-dd HH:mm'}) {
  var dateTime = DateFormat(format).parse(dateLocal, false);
  return dateTime.toUtc();
}

dateFormat(DateTime date, {format = "yyyy-MM-dd"}) {
  final DateFormat formatter = DateFormat(format);
  return formatter.format(date);
}

dateFromString({date, format = "yyyy-MM-dd"}) {
  final DateFormat formatter = DateFormat(format);
  return formatter.format(DateTime.parse(date));
}

hideKeyBoard(BuildContext context) {
  FocusScope.of(context).requestFocus(FocusNode());
}

String filename(File file) {
  var path = file.path;
  var pathArray = path.split("/");
  return pathArray[pathArray.length - 1];
}

String getFileExtension(String? path) {
  if (path != null ) {
    var arr = path.split(".");
    return arr[arr.length - 1];
  } else {
    return "";
  }

}

String monthName(int month, {shortName = true}) {
  var  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  return shortName ?  months[month -1].toString().substring(0, 3)  : months[month - 1];
}

replaceCharAt(String oldString, int index, String newChar) {
  return oldString.substring(0, index) + newChar + oldString.substring(index + 1);
}

formatPhoneNumber(phoneNumber, {prefix = "+256"}) {
  return replaceCharAt(phoneNumber, 0, prefix);
}

formatPhoneNumberWithCode(String phone) {
  phone = phone.replaceAll(RegExp(r'[^\w\s]+'),'');
  if(phone.startsWith("256") && phone.length == 12) {
    return phone;
  }else if(phone.startsWith("0") && phone.length == 10) {
    var re = RegExp(r'\d{1}');
    return phone.replaceFirst(re, '256');
  }else if(phone.startsWith("7") && phone.length == 9) {
    return "256$phone";
  }
  return null;
}

formatMoney(amount, {String currency = "UGX"}) {
  final oCcy =  NumberFormat("#,##0", "en_US");
  amount = amount is String ? double.parse(amount) : amount;
  return currency.length > 1? "$currency ${oCcy.format(amount)}" : "${oCcy.format(amount)}";
}

extension Ex on double {
  double toPrecision(int n) => double.parse(toStringAsFixed(n));
}

Future<void> modalSetState(StateSetter updateState, onValueUpdate) async {
  updateState(() {
    onValueUpdate();
  });
}

showSnackBar(BuildContext context, String message, {seconds = 10, SnackBarAction? action}) {
  final snackBar = SnackBar(
    duration: Duration(seconds: seconds),
    content: Text(message),
    action: action,
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

aweSomeDialog({required context, title = "", desc = "", btnOkPress, btnOkText = "Ok", showCloseIcon = false, btnCancelOnPress, dialogType= DialogType.NO_HEADER, dismissOnTouchOutside: true}) {
  var dialog = AwesomeDialog(
      dismissOnTouchOutside: dismissOnTouchOutside,
      context: context,
      headerAnimationLoop: false,
      dialogType: dialogType,
      title: title,
      desc: desc,
      btnOkOnPress: btnOkPress,
      btnCancelOnPress: btnCancelOnPress,
      btnOkText: btnOkText,
      showCloseIcon: showCloseIcon

  )..show();
}

showToast(String message) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black54,
      textColor: Colors.white,
      fontSize: 16.0);
}