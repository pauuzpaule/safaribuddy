import 'package:flutter/material.dart';
import 'package:safaribuddy/data/auth_obj.dart';
import 'package:safaribuddy/repositories/auth_repository.dart';
import 'package:safaribuddy/util/constants.dart';

class AuthViewModel extends ChangeNotifier {
    bool isLoading = false;
    AuthObj? authObj;
    User? user;
    bool? signUpSuccessful;
    String? signUpError = "";

    bool? otpVerificationSuccessful;
    String? verifyOtpErrorMessage = "";

    bool? loginSuccessful;
    String loginErrorMessage = "";

    Future registerUser(request, {required BuildContext context}) async {
      isLoading = true;
      notifyListeners();

      await AuthRepository(context).registerUser(request).then((value)  {
        isLoading = false;
        if(value  is User) {

          user = value;
          print(user!.toJson());
          signUpSuccessful = true;
        }else {
          signUpSuccessful = false;
          signUpError = value;
        }
        notifyListeners();
      });
    }


    Future verifyOtp(request, {required BuildContext context}) async {
      isLoading = true;
      notifyListeners();

      await AuthRepository(context).verifyOtp(request).then((value)  {
        isLoading = false;
        if(value == kSuccess) {
          otpVerificationSuccessful = true;
        }else {
          otpVerificationSuccessful = false;
          verifyOtpErrorMessage = value;
        }
        notifyListeners();
      });
    }

    Future loginUser(request, {required BuildContext context}) async {
      isLoading = true;
      notifyListeners();

      await AuthRepository(context).loginUser(request).then((value)  {
        isLoading = false;
        if(value  is AuthObj) {
          authObj = value;
          loginSuccessful = true;
        }else {
          loginSuccessful = false;
          loginErrorMessage = value;
        }
        notifyListeners();
      });
    }
}