import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:safaribuddy/data/car.dart';
import 'package:safaribuddy/repositories/car_repository.dart';
import 'package:safaribuddy/util/constants.dart';

class CarViewModel extends ChangeNotifier {
  bool isLoading = false;
  bool carRegisteredSuccessfully = false;
  bool carImageUploaded = false;
  Car? registeredCar;
  String carRegistrationError = "";

  List<Car> carList = [];


  Future createCar(request, {required BuildContext context}) async {
    isLoading = true;
    notifyListeners();

    await CarRepository(context).registerCar(request).then((value)  {
      isLoading = false;
      if(value  is Car) {
        carRegisteredSuccessfully = true;
        registeredCar = value;
      }else {
        carRegisteredSuccessfully = false;
        carRegistrationError = value;
      }
      notifyListeners();
    });
  }


  Future uploadCarImage(carId, File file, {required BuildContext context}) async {
    isLoading = true;
    notifyListeners();

    await CarRepository(context).uploadCarImage(carId, file).then((value)  {
      isLoading = false;
      if(value == kSuccess) {
        carImageUploaded = true;
      }else {
        carImageUploaded = false;
      }
      notifyListeners();
    });
  }

  Future getUserCars({required BuildContext context}) async {
    isLoading = true;
    notifyListeners();

    await CarRepository(context).getUserCars().then((value)  {

      isLoading = false;
      carList = value;
      notifyListeners();

    });
  }

}