import 'package:flutter/cupertino.dart';
import 'package:safaribuddy/data/booked_trip.dart';
import 'package:safaribuddy/data/trip.dart';
import 'package:safaribuddy/repositories/responses/trip_bid_review_response.dart';
import 'package:safaribuddy/repositories/trip_repository.dart';
import 'package:safaribuddy/util/constants.dart';

class TripViewModel extends ChangeNotifier {
  bool isLoading = false;
  bool? tripCreatedSuccessfully;
  String? tripCreationError;
  Trip? createdTrip;

  List<Trip> tripList = [];

  String? reviewBidError;
  Trip? reviewedTrip;
  TripBid? reviewedTripBid;
  bool? tripBidReviewedSuccessfully;

  String? bookTripError;
  bool? tripBookedSuccessfully;

  List<BookedTrip> bookedTrips = [];

  Future createTrip(request, {required BuildContext context}) async {
    isLoading = true;
    notifyListeners();

    await TripRepository(context).createTrip(request).then((value)  {
      isLoading = false;
      if(value  is Trip) {
        tripCreatedSuccessfully = true;
        createdTrip = value;
      }else {
        tripCreatedSuccessfully = false;
        tripCreationError = value;
      }
      notifyListeners();
    });
  }

  Future getAvailableTrips(pickupLat, pickupLng, dropOffLat, dropOffLng, {required BuildContext context}) async {
    isLoading = true;
    notifyListeners();

    await TripRepository(context).getAvailableTrips(pickupLat, pickupLng, dropOffLat, dropOffLng).then((value)  {

      isLoading = false;
      tripList = value;
      notifyListeners();

    });
  }

  Future getUserTrips({required BuildContext context, status}) async {
    isLoading = true;
    notifyListeners();

    await TripRepository(context).getTrips().then((value)  {

      isLoading = false;
      tripList = value;
      notifyListeners();

    });
  }

  Future getBookedTrips({required BuildContext context, status = "pending"}) async {
    isLoading = true;
    notifyListeners();

    await TripRepository(context).getBookedTrips(status: status).then((value)  {

      isLoading = false;
      bookedTrips = value;
      notifyListeners();

    });
  }

  Future reviewTripBid(request, {required BuildContext context}) async {
    isLoading = true;
    notifyListeners();

    await TripRepository(context).reviewBid(request).then((value)  {
      isLoading = false;
      if(value  is TripBidReviewResponse) {
        tripBidReviewedSuccessfully = true;
        reviewedTrip = value.trip;
        reviewedTripBid = value.tripBid;
      }else {
        tripBidReviewedSuccessfully = false;
        reviewBidError = value;
      }
      notifyListeners();
    });
  }

  Future bookTrip(request, {required BuildContext context}) async {
    isLoading = true;
    notifyListeners();

    await TripRepository(context).bookTrip(request).then((value)  {
      isLoading = false;
      if(value == kSuccess) {
        tripBookedSuccessfully = true;
      }else {
        tripBookedSuccessfully = false;
        bookTripError = value;
      }
      notifyListeners();
    });
  }
}