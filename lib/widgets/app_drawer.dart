import 'package:flutter/material.dart';
import 'package:safaribuddy/data/auth_obj.dart';
import 'package:safaribuddy/ui/driver_rides.dart';
import 'package:safaribuddy/util/constants.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/shared_prefs.dart';

class AppDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<AppDrawer> {

  bool isPassenger = true;
  User? currentUser;

  SharedPrefs sharedPreferences = SharedPrefs();


  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      sharedPreferences.getUserType().then((value) {
        setState(() {
          isPassenger = value == kPassenger;
        });
      });

      sharedPreferences.getAuthObj().then((AuthObj? authObj) {
        if(authObj != null) {
          currentUser = authObj.user;
        }
      });

    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    final List _drawerMenu = [
      {
        "icon": Icons.restore,
        "text": "My rides",
        "route": isPassenger? myRides : driverRides
      },
      {
        "icon": Icons.book_online,
        "text": "Booked Rides",
        "route": bookedTripsRoute
      },
      {
        "icon": Icons.directions_car_filled,
        "text": "My cars",
        "route": myCarsRoute
      },
      {
        "icon": Icons.local_activity,
        "text": "Promotions",
        "route": promotionRoutes
      },
      {
        "icon": Icons.star_border,
        "text": "My favourites",
        "route": favouriteScreen
      },
      {
        "icon": Icons.credit_card,
        "text": "My payments",
        "route": paymentsRoute
      },
      {
        "icon": Icons.notifications,
        "text": "Notification",
        "route": notificationScreen
      },
      {
        "icon": Icons.chat,
        "text": "Support",
        "route": chatDetailsRoute

      }
    ];
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(15.0),
            bottomRight: Radius.circular(15.0)),
        color: Colors.white,
      ),
      width: MediaQuery.of(context).size.width -
          (MediaQuery.of(context).size.width * 0.2),
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 25.0,
              ),
              height: 170.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children:  [
                      const CircleAvatar(
                        radius: 30.0,
                        backgroundImage: AssetImage("assets/images/person.png"),
                      ),
                      const Spacer(),
                      tagLabel(Text(isPassenger ? "Passenger" : "Driver", style: const TextStyle(color: Colors.white, fontSize: 11),))
                    ],
                  ),
                  const SizedBox(
                    height: 7.0,
                  ),
                  Text(
                    currentUser != null ? currentUser!.firstname : '',
                    style: const TextStyle(
                      color: Colors.black87,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10,),
                  InkWell(
                    child: Text("Edit Profile", style: TextStyle(color: Colors.blue[600]),),
                    onTap: () {
                        Navigator.of(context).pop();
                        Navigator.pushNamed(context, profileRoute);
                    },
                  )
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.only(
                  top: 20.0,
                ),
                child: ListView(
                  children: _drawerMenu.map((menu) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                        print("route clicked...........");
                        if(menu["route"] != null) {
                          Navigator.pushNamed(context, menu["route"]);
                        }
                      },
                      child: ListTile(
                        leading: Icon(menu["icon"]),
                        title: Text(
                          menu["text"],
                          style: const TextStyle(
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    );
                  }).toList(),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
