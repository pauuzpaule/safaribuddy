import 'package:flutter/material.dart';
import 'package:safaribuddy/data/debitcard.dart';


class CardContainer extends StatelessWidget {
  final DebitCard cardDetail;
  const CardContainer({required this.cardDetail});
  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Card(
      margin: const EdgeInsets.all(0.0),
      child: ListTile(
        contentPadding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
        leading: cardDetail.logo,
        title: Text(
          "**** **** **** ${cardDetail.lastDigits}",
          style: const TextStyle(
            fontSize: 19.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Text(
          "Expires ${cardDetail.expiry}",
          style: const TextStyle(
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    );
  }
}
