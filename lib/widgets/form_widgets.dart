import 'package:flutter/material.dart';
import 'package:safaribuddy/util/color_utils.dart';


backButton({required BuildContext context}) {
  return InkWell(
    onTap: () {
      Navigator.of(context).pop();
    },
    child: Container(
      height: 35,
      width: 35,
      decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white
      ),
      child: Center(
        child: Icon(Icons.arrow_back, color: primaryColor(),),
      ),
    ),
  );
}

textFieldInput(
    {controller,
      initialValue,
      keyboardType,
      validator,
      hintText = "",
      hintTextColor,
      prefixIcon,
      suffixIcon,
      onChanged,
      onTap,
      enabled = true,
      maxLines = 1,
      readOnly = false,
      verticalPadding,
      obscureText = false}) {
  return TextFormField(
    initialValue: initialValue,
    controller: controller,
    style: const TextStyle(color: Colors.black),
    keyboardType: keyboardType,
    validator: validator,
    onChanged: onChanged,
    readOnly: readOnly,
    obscureText: obscureText,
    onTap: onTap,
    maxLines: maxLines,
    textInputAction: TextInputAction.next,
    enabled: true,
    decoration: InputDecoration(
      prefixIcon: prefixIcon,
      suffixIcon: suffixIcon,
      labelText: hintText,
        contentPadding: EdgeInsets.symmetric(
            vertical: verticalPadding ?? 10.0),
      labelStyle: TextStyle(color: hintTextColor ?? primaryColor()),
      hintStyle: TextStyle(color: hintTextColor ?? primaryColor()),
    ),
  );
}

textFieldInputRounded(
    {controller,
      initialValue,
      keyboardType,
      validator,
      hintText = "",
      hintTextColor,
      prefixIcon,
      suffixIcon,
      onChanged,
      onTap,
      enabled = true,
      maxLines = 1,
      readOnly = false,
      verticalPadding,
      obscureText = false}) {
  return TextFormField(
    initialValue: initialValue,
    controller: controller,
    style: const TextStyle(color: Colors.black),
    keyboardType: keyboardType,
    validator: validator,
    onChanged: onChanged,
    readOnly: readOnly,
    obscureText: obscureText,
    onTap: onTap,
    maxLines: maxLines,
    textInputAction: TextInputAction.next,
    enabled: true,
    decoration: InputDecoration(

      prefixIcon: prefixIcon,
      suffixIcon: suffixIcon,
      labelText: hintText,
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.grey, width: 1.0),
        borderRadius: BorderRadius.circular(10.0),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      contentPadding: EdgeInsets.symmetric(
        horizontal: 10,
          vertical: verticalPadding ?? 10.0),
      labelStyle: TextStyle(color: hintTextColor ?? Colors.black54),
      hintStyle: TextStyle(color: hintTextColor ?? Colors.black54),
    ),
  );
}


textAreaRounded(
    {controller,
      initialValue,
      keyboardType,
      validator,
      hintText = "",
      hintTextColor,
      prefixIcon,
      suffixIcon,
      onChanged,
      onTap,
      enabled = true,
      maxLines = 5,
      readOnly = false,
      verticalPadding,
      obscureText = false}) {
  return TextFormField(
    initialValue: initialValue,
    controller: controller,
    style: const TextStyle(color: Colors.black),
    validator: validator,
    onChanged: onChanged,
    readOnly: readOnly,
    obscureText: obscureText,
    onTap: onTap,
    maxLines: maxLines,
    textInputAction: TextInputAction.next,
    minLines: 4,
    keyboardType: TextInputType.multiline,
    enabled: true,
    decoration: InputDecoration(

      prefixIcon: prefixIcon,
      suffixIcon: suffixIcon,
      labelText: hintText,
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.grey, width: 1.0),
        borderRadius: BorderRadius.circular(10.0),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      contentPadding: EdgeInsets.symmetric(
        horizontal: 10,
          vertical: verticalPadding ?? 10.0),
      labelStyle: TextStyle(color: hintTextColor ?? Colors.black54),
      hintStyle: TextStyle(color: hintTextColor ?? Colors.black54),
    ),
  );
}