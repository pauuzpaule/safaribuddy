import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:safaribuddy/util/utils.dart';

class LocationPickerScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<LocationPickerScreen> {

  String location = "";
  double? lat, lng;
  Position? position;
  CameraPosition? _currentCameraPosition;
  final Completer<GoogleMapController> _controller = Completer();
  GoogleMapController? _defaultMapController;

  StreamController<bool>? _streamController;

  bool isVisible = true;

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(0.3130284, 32.4590386),
    zoom: 14.4746,
  );

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;


    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        print("Errror xxxxx---------------> Location permissions are permanently denied, we cannot request permissions.");
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      print("Errror---------------> Location permissions are permanently denied, we cannot request permissions.");
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }

  @override
  void initState() {
    super.initState();
    _streamController = StreamController<bool>();
    buildView();
  }

  buildView () async {
    await _determinePosition().then((value) {
      setState(() {
        lat = value.latitude.toPrecision(6);
        lng = value.longitude.toPrecision(6);
        position = position;
        location = "$lat, $lng";
      });

      _streamController!.add(true);

    });
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: StreamBuilder(
          stream: _streamController!.stream,
          builder: (context, snapdata) {
            if (snapdata.data != null) {
              return  _initView();
            } else {
              return const Center(child: CircularProgressIndicator(strokeWidth: 1,),);
            }
          }),
    );
  }

  _initView() {
    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                _showMap(),
                Center(
                  child: Container(
                    margin: const EdgeInsets.only(bottom: 48),
                    child: const ImageIcon(AssetImage("assets/images/pin.png"), size: 48, color: Colors.deepOrange,),
                  ),
                ),
              ],
            ),
            decoration: const BoxDecoration(color: Colors.green),
          ),
          flex: 8,
        ),
        Expanded(
          child: Container(
            padding: const EdgeInsets.only(top: 5, bottom: 5),
            child: Align(
              alignment: Alignment.center,
              child:  MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.blue[700]!)),
                padding:
                const EdgeInsets.symmetric(vertical: 20.0, horizontal: 64.0),
                child: const Text(
                  "USE LOCATION",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue[700],
                onPressed: () async {
                  LatLng loc = _currentCameraPosition!.target;
                  Navigator.of(context).pop({
                    "lat" : loc.latitude,
                    "lng" : loc.longitude,
                  });
                },
              ),
            ),
          ),
          flex: 2,
        ),
      ],
    );
  }

  _showMap()  {

    return GoogleMap(
      myLocationEnabled: true,
      zoomControlsEnabled: false,
      mapType: MapType.normal,
      initialCameraPosition: _kGooglePlex,
      onCameraIdle: () {

      },
      onCameraMove: (CameraPosition cameraPosition) {
        setState(() {
          _currentCameraPosition = cameraPosition;
        });
      },
      onMapCreated: (GoogleMapController controller) async {
        _controller.complete(controller);
        _defaultMapController = controller;

        await _determinePosition().then((value) {
          setState(() {
            lat = value.latitude.toPrecision(6);
            lng = value.longitude.toPrecision(6);
            position = value;
            location = "$lat, $lng";
          });

          if (position != null) {

            lat = position!.latitude;
            lng = position!.longitude;

            CameraPosition camPosition = CameraPosition(target: LatLng(lat!, lng!), zoom: 15);
            setState(() {
              _currentCameraPosition = camPosition;
            });
            controller.moveCamera(CameraUpdate.newCameraPosition(camPosition));

          }

        });

      },
    );
  }

}