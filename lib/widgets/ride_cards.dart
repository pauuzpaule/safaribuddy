import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:safaribuddy/util/custom_widgets.dart';
import 'package:safaribuddy/util/routes.dart';
import 'package:safaribuddy/util/utils.dart';
import 'package:safaribuddy/widgets/ride_card.dart';


class RideCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RideCard(),
        RideCard(),
        RideCard(),
        RideCard(),
        RideCard(),
        RideCard(),
        RideCard(),
        //_postCard(context)
      ],
    );
  }
  
  _postCard(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Container(
      margin: const EdgeInsets.only(
        top: 10.0,
      ),
      decoration: BoxDecoration(
        border: Border.all(color: _theme.primaryColor),
        borderRadius: BorderRadius.circular(10)
      ),
      child: Card(
        elevation: 0.0,
        child: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Couldn't find a ride ?".toUpperCase(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17.0,
                ),
              ),
              SizedBox(height: 10,),
              ListTile(
                onTap: () {
                  aweSomeDialog(context: context,
                      dialogType: DialogType.SUCCES,
                      title: "",
                      desc: "Trip has been posted.\n We shall contact you shortly.",
                      btnOkPress: () {
                        Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
                      });
                },
                contentPadding: const EdgeInsets.only(
                  left: 0.0,
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.pin_drop,
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    const Flexible(child: Text(
                      "Mulago Rd - Entebe Airport, Wakiso.",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ))
                  ],
                ),
                trailing: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Text(
                      "Time",
                      style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    const Text(
                      "Today, 10:30 AM",
                      style: TextStyle(
                        fontSize: 14.0,
                      ),
                    ),
                  ],
                )
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Post ride if not in available rides', style: TextStyle(color: Colors.grey, fontSize: 12),),
                  formButton(label: "Post My Trip", onPressed: () {
                    aweSomeDialog(context: context,
                        dialogType: DialogType.SUCCES,
                        title: "",
                        desc: "Trip has been posted.\n We shall contact you shortly.",
                        btnOkPress: () {
                          Navigator.pushNamedAndRemoveUntil(context, homeRoute, (route) => false);
                        });
                  }, borderRadius: 10.0, horizontalPadding: 25.0, borderColor: _theme.primaryColor, color: _theme.primaryColor)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
